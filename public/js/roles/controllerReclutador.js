'use strict';

/* Controllers */

adminApp.controller('MenuController',['$scope', '$location', 'SystemResource', function($scope, $location, SystemResource){
	$scope.title = SystemResource.sectionTitle;
	$scope.isActive = function(path) {
		if ($location.path().substr(0, path.length) == path)
			return "active";
		else
			return "";
	};
}]);

adminApp.controller('ReclutadorController',['$scope', '$http', function($scope, $http)
{
	$scope.save = function()
	{

	}
}]);