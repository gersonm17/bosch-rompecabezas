'user strict'

/**
 * Geek Advice Angular App
 */
var adminApp = angular.module('adminApp', ['ngRoute', 'ngResource', 'angular-loading-bar', 'ui.bootstrap', 'ui.bootstrap.progressbar', 'ui.tree']);

/*----------------------------------------------------------------------------------------------------- Routes */
adminApp.config(function($routeProvider){
	$routeProvider
		.when('/security', {
			templateUrl:appConfig.views + '/admin/users/security'
		})
		.when('/config', {
			templateUrl:appConfig.views + '/admin/home/config'
		})
		.when('/styles',{
			templateUrl:appConfig.views + '/styles'
		})
		.when('/dashboard',{
			templateUrl:appConfig.views + '/admin/projects/dashboard'
		})
		.when('/project/:id',{
			templateUrl:appConfig.views + '/admin/projects/detail'
		})
		.when('/project/:id/dynamic/:dynamic_id',{
			templateUrl:appConfig.views + '/admin/projects/dynamic-detail'
		})
		.otherwise({ redirectTo: '/dashboard' });
});

adminApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) { cfpLoadingBarProvider.includeSpinner = false; }]);

/*----------------------------------------------------------------------------------------------------- Directivas */
adminApp.directive('appResizable', function($window) {
	return function($scope, $el, attr) {
		$scope.initializeWindowSize = function() {
			$scope.windowHeight = $window.innerHeight;
			var alto = $window.innerHeight - ($el[0].getBoundingClientRect().top + parseInt(attr.offsetBottom));
			if(alto < 0) alto = 0;
			$el.css({ height:alto+'px' });
			//$el.perfectScrollbar();
			return $scope.windowWidth = $window.innerWidth;
		};
		$scope.initializeWindowSize();
		return angular.element($window).bind('resize', function() {
			$scope.initializeWindowSize();
			//console.log('resize');
			return $scope.$apply();
		});
	};
});

adminApp.directive('appVersion', function() { return { template:'v0.2' }; });
adminApp.directive('itemStatus', function() { return { template:'<span class="label label-info">Hola '+1+'</span>' }; });

function alertCtrl($scope) {
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

}

/**************************************************** Utils */


