'use strict';

/* Controllers */
adminApp.controller('MenuController',['$scope', '$location', 'SystemResource', function($scope, $location, SystemResource){
	$scope.title = SystemResource.sectionTitle;
	$scope.isActive = function(path) {
		if ($location.path().substr(0, path.length) == path)
			return "active";
		else
			return "";
	};
}]);


adminApp.controller('ProjectItemController', function ($scope) {
	$scope.percent = 10;
	$scope.isCollapsed = true;
});

adminApp.controller('ProjectController', ['$scope', 'APIResources', '$routeParams', function($scope, APIResources, $routeParams)
{
	$scope.txtSearch =
	$scope.project = APIResources.projects.select({id:$routeParams.id});
	$scope.id = $routeParams.id;
}]);

adminApp.controller('DynamicController', ['$scope', 'APIResources', '$routeParams', function($scope, APIResources, $routeParams)
{
	$scope.projectID = $routeParams.id;
	$scope.dynamic = APIResources.dynamics.select({id:$routeParams.id}); 
	$scope.id = $routeParams.id;

	$scope.recluiter = false;
	$scope.hide = function(){
		console.log("ocultar");
		$scope.recluiter = !$scope.recluiter;
	};
}]);

adminApp.controller('DashboardController',['$scope', 'APIResources', 'SystemResource', function($scope, APIResources, SystemResource)
{
	SystemResource.sectionTitle = 'Proyectos';
	$scope.filters = {status:1, search:''};
	$scope.searchText = '';
	$scope.errores = [];
	$scope.percent = 10;
	$scope.projects = APIResources.projects.get();

	$scope.search = function()
	{
		console.log($scope.searchText);
	};

	$scope.updateList = function(evt)
	{
		var params = {};

		if($scope.filters.search) params.search = $scope.filters.search;
		if($scope.filters.status) params.status = $scope.filters.status;

		$scope.projects = APIResources.projects.get(params);

		console.log(evt);
	};

}]);

/* End dashboard controller */

adminApp.controller('SecurityController',['$scope', '$http', 'APIResources', 'SystemResource', function($scope, $http, APIResources, SystemResource)
{
	$scope.users = APIResources.users.search();
	SystemResource.sectionTitle = 'Seguridad y usuarios';
	$scope.searchText = '';
	$scope.errores = [];

	//////////////////
	$scope.attributes1 =  [
		{
			"id": 1,
			"title": "NSE",
			"nodes": [
				{
					"id": 1,
					"title": "NSEA1",
					"nodes": []
				}
			]
		},
		{
			"id": 2,
			"title": "Estilo de vida",
			"nodes": []
		},
		{
			"id": 3,
			"title": "Estado Civil",
			"nodes": []
		}
	];

	//////////////////


	$scope.search = function()
	{
		console.log($scope.searchText);
	};

	$scope.save = function()
	{
		$http.post(appConfig.apiURL + '/users/save', $scope.user).success(function (data,status) {
			$scope.user = data.data;
			if ( data.status.code == 400) {
				$scope.errores = data.status.details;
				APIResources.users.search();
			} else {
				$scope.errores = [];
			}
		});

		$scope.users = APIResources.users.search();
	};

	$scope.createUser = function()
	{
		var newUser = {};
		newUser.username = '';
		newUser.email = '';
		newUser.password = '';
		newUser.person = {name:'', last_name:'', dni:''};
		newUser.roles = [{id:-1}];
		$scope.user = newUser;
		console.log(newUser);
	}

	$scope.delete = function()
	{
		if(confirm('¿Quieres eliminar este usuario?, Alguna información se perderá'))
		{
			$http.post(appConfig.apiURL + '/users/delete/' + $scope.user.id, {}).success(function () {
				for(var u in $scope.users.data)
				{
					if($scope.users.data[u].id == $scope.user.id)
					{
						$scope.users.data.splice(u, 1);
						$scope.user = null;
					}
				}
			});
		}
	};

	$scope.changeStatus = function()
	{
		if(confirm('¿Quieres deshabilitar a este usuario?, Este usuario ya no podrá acceder al sistema'))
		{
			$http.post(appConfig.apiURL + '/users/change-status/' + $scope.user.id, {}).success(function (_data)
			{
				$scope.user.disabled = _data.data.disabled;
			});
		}
	};

	$scope.activatePassword = function()
	{
		if($scope.user.password)
			$scope.user.password = undefined;
		else
			$scope.user.password = ' ';
	};

	$scope.close = function()
	{
		$scope.user = null;
	};

	$scope.getSelected = function(_id)
	{
		if($scope.user)
		{
			if($scope.user.id == _id)
				return 'active';
		}

		return '';
	};

	$scope.selectUser = function(_user, _index)
	{
		if(_user.roles.length == 0)
			_user.roles = [{id:-1}];

		$scope.user = _user;

		console.log(_user);
	}

	$scope.default = function()
	{
		return "active";
	}

	/*$scope.update = function(user) {
		$scope.getSelected= angular.copy(user);
	};

	$scope.reset = function() {
		angular.copy($scope.getSelected, $scope.user);
	};

	$scope.reset();*/

}]);

adminApp.controller('ConfigController',['$scope', '$http', function($scope, $http)
{
	$scope.categories = [
  		{
		    "id": 1,
    		"title": "NSE",
    		"nodes": [
    			{
    				"id": 1,
    				"title": "NSEA1",
    				"nodes": []
    			}
    		]
  		},
  		{
    		"id": 2,
    		"title": "Estilo de vida",
    		"nodes": []
  		},
  		{
    		"id": 3,
    		"title": "Estado Civil",
    		"nodes": []
  		}
	];

	$scope.addCategory = function() {
		$scope.categories.push({
			"id":4,
			"title":'Juan',
			"nodes": [
				{
					"title":'Prueba'
				}
			]
		});
	};

	$scope.options = {
		accept: function(sourceNode, destNodes, destIndex) {
			var data = sourceNode.$modelValue;
			var destType = destNodes.$element.attr('data-type');
			//return (data.type == destType); // only accept the same type
			return true;
		},
		dropped: function(event) {
			console.log(event);
			/*var sourceNode = event.source.nodeScope;
			var destNodes = event.dest.nodesScope;
			// update changes to server
			if (destNodes.isParent(sourceNode)
				&& destNodes.$element.attr('data-type') == 'category') { // If it moves in the same group, then only update group
				var group = destNodes.$nodeScope.$modelValue;
				group.save();
			} else { // save all
				$scope.saveGroups();
			}*/
		}
	};

	$scope.save = function()
	{

	}
}]);


var TabsUser = function ($scope) {
	$scope.tabs = [
		{ title:'Dynamic Title 1', content:'Dynamic content 1' },
		{ title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
	];

	$scope.alertMe = function() {
		setTimeout(function() {
			alert('You\'ve selected the alert tab!');
		});
	};
};