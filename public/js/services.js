'use strict';

/* Services */

var APIResources = function($resource)
{
	var factory = {
		users : $resource(appConfig.apiURL + '/users', {}, {
			search: {method:'POST'},
			delete: {method:'POST'}
		}),
		projects : $resource(appConfig.apiURL + '/projects', {}, {
			search: {method:'POST', params:{search:'@search'}, responseType:'json'},
			select: {method:'POST', params:{id:'@id'}, responseType:'json', transformResponse : function(_data){ return _data.data[0]; }}
		}),
		dynamics : $resource(appConfig.apiURL + '/dynamics', {}, {
			search: {method:'POST', params:{search:'@search'}, responseType:'json'},
			select: {method:'POST', params:{id:'@id'}, responseType:'json', transformResponse : function(_data){ return _data.data[0]; }}
		})
	};

	return factory;
};

var SystemResource = function($resource)
{
	var factory = {
		sectionTitle : 'Sistema de gestión'
	};

	return factory;
};

adminApp.factory('APIResources', APIResources);
adminApp.factory('SystemResource', SystemResource);