<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gerson Daniel Aduviri Paredes
 * Date: 30/11/13
 * Time: 06:53 PM
 */
class AppConstants
{
	const SITE_TITLE = 'ReFiPA';
	const DESCRIPTION = 'Sistema de reclutamiento y gestión de proyectos';
	const AUTHOR = 'Geek Advice';

	public function __construct()
	{

	}

}