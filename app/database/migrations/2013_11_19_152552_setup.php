<?php

use Illuminate\Database\Migrations\Migration;

class Setup extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('phone');
            $table->string('city');
            $table->string('gender');
            $table->string('day');
            $table->string('month');
            $table->string('year');
            $table->string('fid');
            $table->string('dni');
            $table->string('data_processing');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('rankings',function($table)
        {
            $table->increments('id');
            $table->string('time');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('images', function($table)
        {
            $table->increments('id');
            $table->string('type');
            $table->string('image');
            $table->softDeletes();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::drop('rankings');
        Schema::drop('questions');
        Schema::drop('clients');
    }

}