<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('clients')->delete();

        Role::create(array(
            'name' => 'Administrador',
            'description' => 'Acceso total al sistema',
            'level' => '10'
        ));

        $user_a = User::create(array(
            'username' => 'rcastillo',
            'email' => 'rcastillo@formax.pe',
            'password' => 'I3OyXRNnDr',
            'verified' => '1',
        ));

        $user_a->roles()->attach(1);

        /*---------------------------------------- Roles */
        Image::create(array(
            'type'=>'PerfectBake',
            'image'=>'images/06.png',

        ));
        Image::create(array(
            'type'=>'PerfectBake',
            'image'=>'images/05.png',

        ));
        Image::create(array(
            'type'=>'PerfectBake',
            'image'=>'images/04.png',
        ));
        Image::create(array(
            'type'=>'4D HotAir',
            'image'=>'images/09.png',
        ));
        Image::create(array(
            'type'=>'4D HotAir',
            'image'=>'images/08.png',

        ));
        Image::create(array(
            'type'=>'4D HotAir',
            'image'=>'images/07.png',

        ));
        Image::create(array(
            'type'=>'PerfectRoast',
            'image'=>'images/01.png',
        ));
        Image::create(array(
            'type'=>'PerfectRoast',
            'image'=>'images/02.png',
        ));
        Image::create(array(
            'type'=>'PerfectRoast',
            'image'=>'images/03.png',
        ));
    }

}