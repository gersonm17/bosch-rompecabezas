<?php

class Person extends \Eloquent {

	protected $fillable = array('client_id',
                                'name',
                                'email_primary',
                                'phone',
                                'province',
                                'city',
                                'country',
                                'address',
                                'code_postal',
                                'site_web',
                                'subtitle',
                                'review'
    );

	protected $guarded = array('id');
    protected $hidden=array('id','updated_at','deleted_at','subtitle');
	protected $softDelete = true;
	public $errors;

	public function isValid($_input)
    {
        $rules = array(
            'name'    => 'required',
            'email_primary' => 'required',
            'phone' => 'required|numeric',
            'province'=>'required',
            'city'=>'required',
            'country'=>'required',
            'address'=>'required',
            'code_postal'=>'required|numeric',
            'site_web'=>'required|url',
            'subtitle'=>'required',
            'review'=>'required'
        );

        $validator = Validator::make($_input, $rules);
        if ($validator->passes())
            return true;

        return false;
	}

	public function clients()
	{
		return $this->belongsTo('Client');
	}


}