<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 11/11/2014
 * Time: 10:44 AM
 */

class Ranking extends  Eloquent
{
    protected $table = 'rankings';
    protected $fillable=array('time','client_id');
    protected $hidden=array('deleted_at','updated_at','client_id');
    protected $protected =array('id');
    public function clients()
    {
        return $this->belongTo('Client');
    }
    /*   public function  options()
        {
            return $this->hasMany('Option');
        }*/
}