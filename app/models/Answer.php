<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 11/11/2014
 * Time: 10:44 AM
 */

class Answer extends  Eloquent
{
    protected $fillable=array('answer','rpta');
    protected $hidden=array('deleted_at','updated_at','question_id');
    protected $protected =array('id');
    public function questions()
    {
        return $this->belongTo('Question');
    }
    /*   public function  options()
        {
            return $this->hasMany('Option');
        }*/
}