<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 11/11/2014
 * Time: 10:44 AM
 */

class Client extends  Eloquent
{
    protected $fillable=array('name',
                                'surname',
                                'city',
                                'dni',
                                'email',
                                'phone',
                                'day','month','year','gender','fid','data_processing','promotion');
    protected $hidden=array('deleted_at','updated_at');
    protected $protected =array('id');

    public function isValid($_input)
    {
        $rules = array(
            'name'    => 'required',
            'surname'=>'required',
            'city' => 'required',
            'dni'=>'required|numeric|digits:8',
            'email'=>'required',
            'phone'=>'required|numeric',
            'day' => 'required',
            'month' => 'required',
            'year' => 'required',
            'gender' => 'required',

        );
        $friendly_names = array(
        'name' => 'Nombre',
        'surname' => 'Apellido',
            'city' => 'Ciudad',
            'dni'=>'DNI',
            'email'=>'EMAIL',
            'phone'=>'Telefono',
            'day' => 'Dia',
            'month' => 'Mes',
            'year' => 'Año',
            'gender' => 'Genero',
            );
        $validator = Validator::make($_input, $rules);
        $validator->setAttributeNames($friendly_names);
        return $validator;
    }
    public function rankings()
    {
        return $this->hasMany('Ranking');
    }
    public function getCreatedAtAttribute($value)
    {
        $dateini=date_create($value);
        $date=date_format($dateini,"d/m/Y H:i:s");
        return $date;

    }

    public function setNameAttribute($value)
    {
            $this->attributes['name'] = strtoupper($value);
    }

    public function setSurnameFatherAttribute($value)
    {
            $this->attributes['surname_father'] = strtoupper($value);
    }

    public function setSurnameMotherAttribute($value)
    {
            $this->attributes['surname_mother'] = strtoupper($value);
    }

    public function setEmailAttribute($value)
    {
            $this->attributes['email'] = strtoupper($value);
    }
/*   public function  options()
    {
        return $this->hasMany('Option');
    }*/

    


}