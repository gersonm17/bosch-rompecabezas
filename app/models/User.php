<?php

	use Toddish\Verify\Models\User as VerifyUser;

	class User extends VerifyUser
	{
		protected $table ='users';
		protected $fillable = array('username', 'email', 'password', 'verified');
		protected $hidden = array('password', 'salt');
		protected $guarded = array('id', 'remember_token');

		public $errors;

		public function isValid($_input)
		{
			$rules = array(
				'username' => 'required',
				'email'    => 'required|email',
			);

			$validator = Validator::make($_input, $rules);

			if ($validator->passes())
			{
				return true;
			}
			//$this->errors = $validator->errors();
			else
			return false;
		}


        public  function clients()
        {
            return $this->hasOne('Client');
        }

        public function mails()
        {
            return $this->hasOne('Mail');
        }

	}