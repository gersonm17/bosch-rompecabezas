<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 11/11/2014
 * Time: 10:44 AM
 */

class Image extends  Eloquent
{
    protected $fillable=array('type');
    protected $hidden=array('deleted_at','updated_at');
    protected $protected =array('id');

    /*   public function  options()
        {
            return $this->hasMany('Option');
        }*/

}