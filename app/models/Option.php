<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 11/11/2014
 * Time: 10:44 AM
 */

class Option extends  Eloquent
{
    protected $fillable=array('question_id','answer','num_rptas','client_id');
    protected $hidden=array('deleted_at','updated_at','question_id');
    protected $protected =array('id');
  /*  public function  clients()
    {
        return $this->belongTo('Client');
    }
    public function  answers()
    {
        return $this->belongTo('Answer');
    }
    public function  questions()
    {
        return $this->belongTo('Question');
    }*/
}