<?php

class HomeController extends BaseController
{

    protected $layout = 'ci.tpl_site';

    public function index()
    {
        $this->layout->content = View::make('home');
    }

    public function preguntas()
    {
        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );

            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            if ($fbUserId)
            {
       ///

            }
            return Redirect::to('/');

        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }


    public function respuestas()
    {
        try{
            $time=Input::get('time');
            Session::put('time',$time);
            return $time;
        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }

    public function login()
    {
        $this->layout->content = View::make('ci.login');
    }

    public function token()
    {
        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );
            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            $fbLoginUrl = false;
            Session::put('try', 0);
            // El usuario no tiene sesión activa en facebook...

            if ($fbUserId) {
                $me = $facebook->api('/' . $fbUserId);
                $me['image'] = 'https://graph.facebook.com/' . $me['id'] . '/picture?type=large';
                /*     $array = [
                         "name" => $me['first_name'].' '.$me['last_name'],
                         "photo" => 'https://graph.facebook.com/'.$me['username'].'/picture?type=large',
                     ];*/
                $porciones = explode(" ", $me['name']);
                if(count($porciones)>0)
                {
                    $me['first_name']=$porciones[0];
                    if(count($porciones)>3)
                    {
                        $me['last_name']=$porciones[2].' '.$porciones[3];
                        $me['first_name']=$porciones[0].' '.$porciones[1];
                    }
                    else{
                        if(count($porciones)==2)
                        {
                            $me['last_name']=$porciones[1];
                        }
                    }
                }
                $this->layout->content = View::make('star', array(
                    'me' => $me,
                ));
            }


            $fbLoginUrl = $facebook->getLoginUrl(array(
                'scope' => 'email, public_profile, publish_actions',
                'redirect_uri' => url('facebook'),
            ));
            return $fbLoginUrl;
        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }

    public function iniciar()
    {
        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );
            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            if ($fbUserId)
            {
                $userProfile = $facebook->api('/' . $fbUserId, 'GET');
                $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                // Buscar usuario con el Email o ID de Facebook
                $porciones = explode(" ", $userProfile['name']);
                if(count($porciones)>0)
                {
                    $userProfile['first_name']=$porciones[0];
                    if(count($porciones)>3)
                    {
                        $userProfile['last_name']=$porciones[2].' '.$porciones[3];
                        $userProfile['first_name']=$porciones[0].' '.$porciones[1];
                    }
                    else{
                        if(count($porciones)==2)
                        {
                            $userProfile['last_name']=$porciones[1];
                        }
                    }
                }
                Session::put('fib', $fbUserId);
                $p1 = mt_rand(1,9);
                $imagen=Image::find($p1);
                return View::make('star', array(
                    'me' => $userProfile,
                    'imagen'=>$imagen
                ));
            }
            // Error, el usuario no permitio la aplicación de facebook u otro problema.
            return Redirect::to('/');

        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }

    public function registrate()
    {
        try{
            if(\Session::has('time')) {
                $config = array(
                    'appId' => Config::get('facebook.appId'),
                    'secret' => Config::get('facebook.secret'),
                    'fileUpload' => Config::get('facebook.fileUpload'),
                );
                $facebook = new Facebook($config);
                $fbUserId = $facebook->getUser();
                if ($fbUserId) {
                    $rcount = true;
                    $c = Client::where('fid', '=', $fbUserId)->first();
                    $userProfile = $facebook->api('/' . $fbUserId, 'GET');
                    $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                    if (count($c) > 0) {

                        $ranking = new \Ranking();
                        $ranking->client_id = $c->id;
                        $ranking->time = \Session::get('time');
                        $ranking->save();
                        $rankings=  DB::table('rankings') ->join('clients', 'rankings.client_id', '=', 'clients.id')->orderBy('time', 'asc')->groupBy('client_id')->get(['fid','name','surname','client_id', DB::raw('MIN(time) as time')]);
                        return View::make('ranking', array(
                            'ranking' => $rankings,
                            'me' => $userProfile
                        ));

                    } else {
                        $porciones = explode(" ", $userProfile['name']);
                        if (count($porciones) > 0) {
                            $userProfile['first_name'] = $porciones[0];
                            if (count($porciones) > 3) {
                                $userProfile['last_name'] = $porciones[2] . ' ' . $porciones[3];
                                $userProfile['first_name'] = $porciones[0] . ' ' . $porciones[1];
                            } else {
                                if (count($porciones) == 2) {
                                    $userProfile['last_name'] = $porciones[1];
                                }
                            }
                        }
                        return View::make('formulario', array(
                            'me' => $userProfile,
                        ));
                    }
                }
            }
            // Error, el usuario no permitio la aplicación de facebook u otro problema.
            return Redirect::to('/');

        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }

    public function register()
    {
        try{
            if(\Session::has('time')) {
                $data = Input::all();
                if (Session::has('fib')) {
                    $fib = Session::get('fib');
                    $config = array(
                        'appId' => Config::get('facebook.appId'),
                        'secret' => Config::get('facebook.secret'),
                        'fileUpload' => Config::get('facebook.fileUpload'),
                    );
                    $facebook = new Facebook($config);
                    if ($fib) {
                        $userProfile = $facebook->api('/' . $fib, 'GET');
                        $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                        $porciones = explode(" ", $userProfile['name']);
                        if (count($porciones) > 0) {
                            $userProfile['first_name'] = $porciones[0];
                            if (count($porciones) > 3) {
                                $userProfile['last_name'] = $porciones[2] . ' ' . $porciones[3];
                                $userProfile['first_name'] = $porciones[0] . ' ' . $porciones[1];
                            } else {
                                if (count($porciones) == 2) {
                                    $userProfile['last_name'] = $porciones[1];
                                }
                            }
                        }
                        $c = new Client();
                        $validator = $c->isValid($data);
                        if ($validator->fails()) {
                            return Redirect::to('registrate')->withErrors($validator);
                        }
                        $client = new Client();
                        $client->fill($data);
                        $client->fid = $fib;
                        $client->save();
                        if (Session::has('time')) {
                            $ranking = new \Ranking();
                            $ranking->client_id = $client->id;
                            $ranking->time = \Session::get('time');
                            $ranking->save();
                        }
                        $access_token = $facebook->getAccessToken();
                        $params = array(
                                // this is the access token for Fan Page
                                "access_token" => "$access_token",
                                "message" => "¡Participa tú también!",
                                "link" => "bOSCH-HOME.PE",
                                "picture" => URL::to('images/POSTLINK-FACEBOOK_ROMPECABEZAS_02.png'),
                                "name" => "¡Increíbles premios te esperan!",
                                "caption" => "BOSCH-HOME.PE",
                                "description" => "¡Completé el rompecabezas participando por el horno Serie 8 y los gift cards de Bosch! Juega aqui."
                            );

                            try {
                                // 466400200079875 is Facebook id of Fan page https://www.facebook.com/pontikis.net
                                $ret = $facebook->api('/' . $fib . '/feed', 'POST', $params);
                                //echo 'Successfully posted to Facebook Fan Page';
                            } catch (Exception $e) {
                             //   echo $e->getMessage();
                            }

                        return View::make('participaste', array(
                            'me' => $userProfile
                        ));
                    }
                }
            }
          return Redirect::to('/');

        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }

    public function oportunidades()
    {
        try{
            if (Session::has('fib')) {
                $fib = Session::get('fib');
                $config = array(
                    'appId' => Config::get('facebook.appId'),
                    'secret' => Config::get('facebook.secret'),
                    'fileUpload' => Config::get('facebook.fileUpload'),
                );
                $facebook = new Facebook($config);
                if ($fib) {
                    $c = Client::where('fid', '=', $fib)->first();
                    $hoy = date("Y-m-d");
                    if($c->option_date!=$hoy)
                    {
                        $c->option_friends=2;
                    }
                    if($c->option_friends==2)
                    {
                        $c->option_friends=1;

                    }
                    $c->save();
                }
                else
                {
                    return Redirect::to('/');
                }
            }
            return Redirect::to('/');
        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }
    public function participaste()
    {

        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );
            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            if ($fbUserId)
            {
                $userProfile = $facebook->api('/' . $fbUserId, 'GET');
                $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                $porciones = explode(" ", $userProfile['name']);
                if(count($porciones)>0)
                {
                    $userProfile['first_name']=$porciones[0];
                    if(count($porciones)>3)
                    {
                        $userProfile['last_name']=$porciones[2].' '.$porciones[3];
                        $userProfile['first_name']=$porciones[0].' '.$porciones[1];
                    }
                    else{
                        if(count($porciones)==2)
                        {
                            $userProfile['last_name']=$porciones[1];
                        }
                    }
                }
                $rankings=  DB::table('rankings') ->join('clients', 'rankings.client_id', '=', 'clients.id')->orderBy('time', 'asc')->groupBy('client_id')->get(['fid','name','surname','client_id', DB::raw('MIN(time) as time')]);
                return View::make('ranking', array(
                    'ranking' => $rankings,
                    'me' => $userProfile
                ));

            }
            else
            {
                return Redirect::to('/');
            }

        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }
    public function results()
    {
        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );
            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            if ($fbUserId)
            {

                $userProfile = $facebook->api('/' . $fbUserId, 'GET');
                $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                    $porciones = explode(" ", $userProfile['name']);
                if(count($porciones)>0)
                {
                    $userProfile['first_name']=$porciones[0];
                    if(count($porciones)>3)
                    {
                        $userProfile['last_name']=$porciones[2].' '.$porciones[3];
                        $userProfile['first_name']=$porciones[0].' '.$porciones[1];
                    }
                    else{
                        if(count($porciones)==2)
                        {
                            $userProfile['last_name']=$porciones[1];
                        }
                    }
                }
                $c = Client::where('fid', '=', $fbUserId)->count();
                $op=0;
                if($c>0)
                {
                    $op=1;
                }
                // Buscar usuario con el Email o ID de Facebook

                return View::make('resultado', array(
                    'me' => $userProfile,
                    'op'=>$op
                ));

            }
            else
            {
                return Redirect::to('/');
            }
        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }
    public function video()
    {
        try{
            $config = array(
                'appId' => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret'),
                'fileUpload' => Config::get('facebook.fileUpload'),
            );

            $facebook = new Facebook($config);
            $fbUserId = $facebook->getUser();
            if ($fbUserId)
            {

                $userProfile = $facebook->api('/' . $fbUserId, 'GET');
                $userProfile['image'] = 'https://graph.facebook.com/' . $userProfile['id'] . '/picture?type=large';
                $questions='';
                $time='';
                $porciones = explode(" ", $userProfile['name']);
                if(count($porciones)>0)
                {
                    $userProfile['first_name']=$porciones[0];
                    if(count($porciones)>3)
                    {
                        $userProfile['last_name']=$porciones[2].' '.$porciones[3];
                        $userProfile['first_name']=$porciones[0].' '.$porciones[1];
                    }
                    else{
                        if(count($porciones)==2)
                        {
                            $userProfile['last_name']=$porciones[1];
                        }
                    }
                }
                $c = Client::where('fid', '=', $fbUserId)->first();
                $rcount=true;
                $boton='register';
                if (count($c) > 0) {
                    $client = Client::find($c->id);
                    $boton='continuar';
                    $hoy = date("Y-m-d");
                    if($client->options<3 || $client->option_date!= $hoy || $client->option_friends==1 )
                    {
                        $rcount=true;

                    }
                    else
                    {
                        $rcount=false;
                    }

                }
                if($rcount==true) {
                    if (Session::has('questions') && Session::has('time')) {
                        $questions = Session::get('questions');
                        $time = Session::get('time');
                    }
                    else
                    {
                        return Redirect::to('/');
                    }
                    // Buscar usuario con el Email o ID de Facebook

                    return View::make('video', array(
                        'me' => $userProfile,
                        'questions' => $questions,
                        'time' => $time,
                        'boton'=>$boton
                    ));
                }
                else
                {
                    $rankings= DB::table('rankings')
                        ->distinct()
                        ->groupBy('client_id')
                        ->orderBy(DB::raw('MAX(num_rptas)'),'desc')
                        ->orderBy('time','asc')->get(['client_id']);
                    $count=0;
                    $r=[];
                    foreach($rankings as $ranking){
                        $r[$count]= DB::table('rankings')
                            ->join('clients', 'rankings.client_id', '=', 'clients.id')
                            ->orderBy('num_rptas','desc')
                            ->orderBy('time','asc')
                            ->where('rankings.client_id','=',$ranking->client_id)
                            ->first();
                        $count++;
                    }
                    return View::make('ranking', array(
                        'ranking' => $r,
                        'me'=> $userProfile,
                        'intento'=>5
                    ));
                }
            }
            else{
                return Redirect::to('/');
            }
        }
        catch(Exception $e)
        {
            return Redirect::to('/');
        }
    }
    public function redirectfb()
    {
        return View::make('facebookredirect');
    }
}