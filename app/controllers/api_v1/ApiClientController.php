<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/11/2014
 * Time: 10:21 AM
 */

class ApiClientController extends ApiBaseController
{
    public function index()
    {
        return View::make('admin.home');
    }

    public function newclient()
    {
        $r = new ApiResponse();
        $_error = false;
        $entry = null;
        $data = Input::all();
        $entry = new Client;
        if($entry->isValid($data))
        {
            $entry->fill($data);
            $entry->save();
        }
        else
        {
            $r->status->setStatus(Status::STATUS_ERROR_PARAMETROS);
        }
        return Response::json($r, $r->status->code);
    }



    public function download()
    {
        
        $r = new ApiResponse();
        $_error = false;
        $entry = null;

        $entries=Client::where('temp','=',1)->get(array('name as Nombre','surname as Apellido','phone as Telefono','gender as Genero','dni as Dni','email as Email','city as Ciudad','day as Dia','month as Mes','year as Año','time_total as tiempo_total','terms as Terminos','promotion as Promociones','data_processing as Tratamiento_datos','options as Opciones','fid as Facebook_id', 'created_at'));
        $entry = array();
        for ($count = 0; $count < sizeof($entries) ;$count++){
            $entry[$count]['Nombre'] = $entries[$count]->Nombre;
            $entry[$count]['Apellido'] = $entries[$count]->Apellido;
            $entry[$count]['Telefono'] = $entries[$count]->Telefono;
            $entry[$count]['Genero'] = $entries[$count]->Genero;
            $entry[$count]['Dni'] = $entries[$count]->Dni;
            $entry[$count]['Email'] = $entries[$count]->Email;
            $entry[$count]['Ciudad'] = $entries[$count]->Ciudad;
            $entry[$count]['Dia'] = $entries[$count]->Dia;
            $entry[$count]['Mes'] = $entries[$count]->Mes;
            $entry[$count]['Año'] = $entries[$count]->Año;
            $entry[$count]['tiempo_total'] = $entries[$count]->tiempo_total;
            $entry[$count]['Terminos'] = $entries[$count]->Terminos;
            $entry[$count]['Promociones'] = $entries[$count]->Promociones;
            $entry[$count]['Tratamiento_datos'] = $entries[$count]->Tratamiento_datos;
            $entry[$count]['Facebook_id'] = $entries[$count]->Facebook_id;
            $entry[$count]['Opciones'] = $entries[$count]->Opciones;
            $entry[$count]['creacion'] = $entries[$count]->created_at;
        }
        if(count($entries)==0)
        {
            $r->status->code='220';
            $r->status->description='No se encontraron registros';
            return Response::json($r, $r->status->code);
        }
        else {
            Excel::create('Reporte clientes bosch', function ($excel) use ($entry){
                $excel->sheet('Clientes', function ($sheet) use ($entry){
                    $count=count($entry)+3;
                    $sheet->setBorder('B3:R'.$count,'thin',false,'#8684FF');

                    $sheet->cells('B3:R3', function($cells){
                        $cells->setBackground('#AECBEC');
                    });

                    $sheet->cells('B4:R'.$count, function($cells){
                        $cells->setBackground('#E3F0FF');
                    });
                    $sheet->fromArray($entry, true, 'B3', true, true);
                });
            })->export('xls');
        }
    }

} 