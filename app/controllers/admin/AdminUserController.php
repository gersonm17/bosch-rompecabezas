<?php

class AdminUserController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$users = User::get();
		$roles = Role::get();

		return View::make('admin.users', array(
			'users'=> $users,
			'roles'=> $roles
		));
	}

	public function getSecurity()
	{
		$roles = Role::get();

		return View::make('admin.security', array(
			'roles'=> $roles
		));
	}
}