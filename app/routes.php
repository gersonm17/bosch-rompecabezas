<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::any('/', 'HomeController@index');
Route::any('login', 'LoginController@index');
//Route::any('preguntas', 'HomeController@preguntas');
Route::any('results', 'HomeController@results');
Route::any('respuestas', 'HomeController@respuestas');
Route::any('iniciar', 'HomeController@iniciar');
Route::controller('users','UserController');
Route::any('facebook', 'HomeController@iniciar');
Route::any('registrate', 'HomeController@registrate');
Route::any('register', 'HomeController@register');
Route::any('oportunidades', 'HomeController@oportunidades');
Route::any('ranking','HomeController@participaste');
Route::any('token','HomeController@token');
Route::any('video','HomeController@video');
Route::any('redirectfb', 'HomeController@redirectfb');

//------------------------------------------------------------------------------- Admin

Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function()
{
	Route::get('home', 'ApiClientController@index');
	Route::get('download', 'ApiClientController@download');
});

//------------------------------------------------------------------------------- API

Route::group(array('prefix' => 'api/v1'), function() //con autenticación
{
    Route::post('client/new', 'ApiClientController@newclient');
});
















