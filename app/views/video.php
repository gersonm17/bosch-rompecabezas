<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Verdad o Mito</title>
	<link rel="stylesheet" type="text/css" href="css/reveal.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/tooltipster.css" />
	<link rel="stylesheet" href="css/jquery.remodal.css">
    <script src="https://connect.facebook.net/es_ES/all.js"></script>
    <script>
        function sharefacebook()
        {
            FB.init({
                appId:'1485655285067130',
                cookie:true,
                status:true,
                xfbml:true
            });
            var nu= 1;
            switch (nu)
            {
                case 1:
                    img='https://bosch-promociones.formaxlabs.com/images/POST-AUTOPUBLICACIÓN-04-COMPARTIR-RANKING.jpg';
                    capt='¡Participa tú también!';
                    text='Juega, reta a tus amigos y entérate de los increibles beneficios que tenemos para ti.';
                    break;
            }
            FB.ui({
                method: 'feed',
                picture: img,
                link: 'http://www.bosch-home.pe/trivia-verdad-o-mito.html',
                name:'¡Participa tú también!',
                caption: capt,
                description: text
            },function(response) {
               // if (response && !response.error_code) {
                    registrarse();
                //}
            });
        }
    </script>
</head>
<body>
	<div class="content">
		<div class="banner">
			<div class="star">
				<div class="perfil">
					<div class="profile-photo-circular">
						<img src="<?php echo (isset($me['image'])?$me['image']:'') ;?>">

					</div>
				</div>
				<div class="msjstar" style="top:117px;">

					<p style="font-size:28px;font-family: 'latobold';">Muy bien, <?php echo (isset($me['first_name'])?$me['first_name']:'') ;?>!</p>
					<p style="font-size:16px;font-family: 'latoregular';">Entérate de los increíbles beneficios<br><span style="font-family: 'latolight';">de los nuevos hornos eléctricos Serie 8</span></p>
				</div>
				<a href="http://www.bosch-home.pe/serie-8-hornos.html" target="_blank">
					<div class="btnstar" style="left:346px;top:210px;">
						<p style="margin-top: 40px;margin-left: 18px;color: white;font-size: 14px;font-family: 'latolight';cursor:pointer;">Conoce más<br>de la <span style="font-family: 'latobold';">Serie 8</span></p>
					</div>
				</a>
			</div>
		</div>
		<div class="game">
			<p style="text-align:center;font-size:25px;font-family: 'latoregular';margin:42px 0;">Resumen del juego</p>
			<table class="video" cellspacing="4">
                <?php $count=0; foreach($questions as $question){ $count=$count+1; if($question['rptacorrect']=='verdad'){ ?>
                    <tr>
                        <td class="opre"> <?php echo $count; ?></td>

                        <?php if($question['type']=='PerfectBake'){ ?>
                            <td class="repre verdad tooltipstered PerfectBake">Verdad</td>
                        <?php }elseif($question['type']=='4D HotAir'){ ?>
                            <td class="repre verdad tooltipstered HotAir"  >Verdad</td>
                        <?php }elseif($question['type']=='PerfectRoast'){ ?>
                            <td class="repre verdad tooltipstered PerfectRoast" >Verdad</td>
                        <?php }else{ ?>
                            <td class="repre verdad tooltipstered other">Verdad</td>
                        <?php } ?>
                    </tr>
                <?php }else{ ?>
                    <tr>
                        <td class="opre"><?php echo $count; ?></td>
                        <?php if($question['type']=='PerfectBake'){ ?>
                            <td class="repre mito tooltipstered PerfectBake" >Mito</td>
                        <?php }elseif($question['type']=='4D HotAir'){ ?>
                            <td class="repre mito tooltipstered HotAir"  >Mito</td>
                        <?php }elseif($question['type']=='PerfectRoast'){ ?>
                            <td class="repre mito tooltipstered PerfectRoast" >Mito</td>
                        <?php }else{ ?>
                            <td class="repre mito tooltipstered other">Mito</td>
                        <?php } ?>
                    </tr>
                <?php }} ?>

			</table>
			<p style="text-align:center;font-family: 'latolight';font-size:21px;margin-top:52px;">Para participar por uno de los exclusivos libros<br><span style="font-family: 'latobold';">"Proyecto + Gourmet”</span> primero debes registrarte.</p>
			<div style="width:100%;text-align:center;margin-top: 10px;">
                <?php if($boton=='register'){ ?>
                    <a onclick="sharefacebook()" class="btnface" style="padding-left: 30px;padding-right: 30px;font-family:'latoregular';font-size:16px;"> REGÍSTRATE Y COMPARTE</a>
                <?php }else{ ?>
                    <a onclick="registrarse()" class="btnface" style="padding-left: 30px;padding-right: 30px;font-family:'latoregular';font-size:16px;"> CONTINUAR</a>
                <?php } ?>
			</div>
			<div class="buttons">
				<a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
					<img src="images/terminos.png">
				</a>
				<a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
					<img src="images/participar.png">
				</a>
				<a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
					<img src="images/premio.png">
				</a>
			</div>
			<div class="rd">
				<p>RD 25145267 - GDAIA</p>
			</div>
		</div>
	</div>
	<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
		<p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
		<div class="cont-scroll">
			<ul class="letters" style="font-size: 13px;">
					<li>Duración: 3 semanas</li>
					<li>Fecha de Inicio: 31 de Agosto</li>
					<li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
					<li>Fecha de sorteo: 22 de Setiembre</li>
					<li>Publicación de ganadores: 23 de Setiembre</li>
					<li>Premios
						<ul>
							<li>3 libros "Proyecto + Gourmet"</li>
						</ul>
					</li>
					<li>Descripción del mecanismo y las condiciones del sorteo:
						<ul>
							<li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
							<li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
						</ul>
					</li>
					<li>
						Cómo participar
						<ul>
							<li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
							<li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
							<li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
								<ul>
									<li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
								</ul>
							</li>
							<li>
								Cómo tener más opciones de jugar:
								<ul>
									<li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
									<li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
								</ul>
							</li>
						</ul>
					</li>
					<li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
					<li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
					<li>El concurso es válido únicamente para Lima.</li>
					<li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
					<li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
					<li>Fecha de canje: 23 de Setiembre</li>
					<li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
					<li>Promoción solo válida para Lima.</li>
					<li>Requisito para hacer efectivo el premio DNI original y copia.</li>
				</ul>
		</div>
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
		<div class="cont-scroll">
			<p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
			<p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
			<ol>
				<li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
				<li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
				<li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
			</ol>
			<ul>
				<li>
					Cómo tener más opciones de jugar:<br><br>
					<ul>
						<li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
						<li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
					</ul>
				</li>
			</ul>
		</div>
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<div id="premiosof" class="reveal-modal">
		<p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
		<img src="images/premios.png" style="width: 500px;margin-top: 50px;margin-left: 20px;">
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<div class="remodal func1 viewpic" data-remodal-id="modal1">
	    <iframe width="630" height="400" id="vifun1" src="https://www.youtube.com/embed/qwPl6FxARds?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
	<div class="remodal func2 viewpic" data-remodal-id="modal2">
	    <iframe width="630" height="400" id="vifun2" src="https://www.youtube.com/embed/WkYuXolRSlA?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
	<div class="remodal func3 viewpic" data-remodal-id="modal3">
	    <iframe width="630" height="400" id="vifun3" src="https://www.youtube.com/embed/57fNq8zDw6k?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
	<div class="remodal func4 viewpic" data-remodal-id="modal4">
	    <iframe width="630" height="400" id="vifun4" src="https://www.youtube.com/embed/zw2cTtGuJog?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.0.min.js"></script>
	<script src="js/jquery.reveal.js"></script>
	<script type="text/javascript" src="js/jquery.tooltipster.js"></script>
	<script src="js/all.js"></script>
	<script src="js/jquery.remodal.js"></script>
	<script type="text/javascript">

		$(document).ready(function() {
			$('.PerfectBake').tooltipster({
			   animation: 'grow',
	   			delay: 200,
	   			position: 'right',
	   			theme: 'tooltipster-default',
	   			touchDevices: true,
	   			interactive: true,
	   			trigger: 'hover',
	   			content: $('<a href="#modal1"><img src="images/video.jpg"></a><p style="width:235px;background:#004d7e;color:white;font-family:latolight;text-align:center;padding: 15px 8px;">No tienes de qué preocuparte, gracias a la función <span style="font-family:latoregular;">PerfectBake</span> tus pasteles serán perfectos.</p>')
			});
			$('.PerfectRoast').tooltipster({
			   animation: 'grow',
	   			delay: 200,
	   			position: 'right',
	   			theme: 'tooltipster-default',
	   			touchDevices: true,
	   			interactive: true,
	   			trigger: 'hover',
	   			content: $('<a  href="#modal2"><img src="images/video.jpg"></a><p style="width:235px;background:#004d7e;color:white;font-family:latolight;text-align:center;padding: 15px 8px;">No tienes de qué preocuparte, gracias a la función <span style="font-family:latoregular;">PerfectBake</span> tus pasteles serán perfectos.</p>')
			});
			$('.other').tooltipster({
			   animation: 'grow',
	   			delay: 200,
	   			position: 'right',
	   			theme: 'tooltipster-default',
	   			touchDevices: true,
	   			interactive: true,
	   			trigger: 'hover',
	   			content: $('<a  href="#modal3"><img src="images/video.jpg"></a><p style="width:235px;background:#004d7e;color:white;font-family:latolight;text-align:center;padding: 15px 8px;">No tienes de qué preocuparte, gracias a la función <span style="font-family:latoregular;">PerfectBake</span> tus pasteles serán perfectos.</p>')
			});
			$('.HotAir').tooltipster({
			   animation: 'grow',
	   			delay: 200,
	   			position: 'right',
	   			theme: 'tooltipster-default',
	   			touchDevices: true,
	   			interactive: true,
	   			trigger: 'hover',
	   			content: $('<a  href="#modal4"><img src="images/video.jpg"></a><p style="width:235px;background:#004d7e;color:white;font-family:latolight;text-align:center;padding: 15px 8px;">No tienes de qué preocuparte, gracias a la función <span style="font-family:latoregular;">PerfectBake</span> tus pasteles serán perfectos.</p>')
			});
		});
	</script>	
</body>
</html>