<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Serie 8</title>
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>
<div class="contentpri">
    <div class="banner">
        <div class="star">
            <div class="perfil" style="background: url('<?php echo (isset($me['image'])?$me['image']:'') ;?>') center;background-size: cover;"></div>
            <div class="msjstar">
                <p style="font-size:25px;font-family: 'latobold';">Los hornos Serie 8 te facilitan la vida</p>
                <p style="font-size:16.86px;font-family: 'latolight';"> ¡Entérate más aquí!</p>
                <div class="coma">
                    <a href="http://www.bosch-home.pe/serie-8-hornos.html" target="_blank" class="btnface3" style="font-size:13px;">Conoce más<br>de la <strong>serie 8</strong></a>
                </div>
            </div>
        </div>
    </div>
    <div class="game">
        <div style="height: 543px;">
            <img src="img/hazul.png" style="float:left;">
            <div style="width:405px;float: left;text-align:center;position:relative;">
                <div style="margin-top:170px;">
                    <p style="font-family: 'latobold';font-size:22px;">¡Gracias por participar en<br> el reto Serie 8!
                        <br></p>
                    <a href="ranking" class="btnface2" style="font-size:18px;">Mira tu ranking aquí</a>
                </div>
            </div>
        </div>
        <div class="buttons">
            <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
                <img src="img/terminos.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
                <img src="img/participar.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
                <img src="mg/premio.png">
            </a>
        </div>
        <div class="rd">
            <p>RD 25145267 - GDAIA</p>
        </div>
    </div>
</div>
<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll2">
        <ul class="letters" style="font-size: 13px;">
            <li>Denominación de la promoción: “Reto Serie 8”</li>
            <li>Ámbito que abarca la promoción: Sólo Lima Metropolitana y Callao.</li>
            <li>Duración de la promoción: 28 días calendarios</li>
            <li>Fecha de inicio de la promoción: 15 de Octubre de 2015.</li>
            <li>Fecha de finalización de la promoción: 12 de Noviembre de 2015 a las 0:00hrs. Debiendo tenerse presente que el sorteo se realizará el 12 de Noviembre de 2015 a las 16:00hrs.</li>
            <li>MECANISMO Y CONDICIONES DE LA PROMOCION:
                <ul>
                    <li>Participarán en el sorteo todas las personas que durante el periodo promocional, ingresen a la página web [http://www.bosch-home.pe/reto-serie8.html] y se conecten con su perfil de Facebook.</li>
                    <li>Luego de ello, el participante deberá armar un rompecabezas en el menor tiempo posible. Las imágenes de rompecabezas serán seleccionadas aleatoriamente por la empresa organizadora.</li>
                    <li>Una vez que el participante arme el rompecabezas, deberá, completar el formulario con los siguientes datos: Nombres, Apellidos, Fecha de nacimiento, Teléfono, email, DNI, sexo y ciudad.</li>
                    <li>Luego de completar el formulario, el participante estará registrado en la promoción. Los participantes podrán volver a jugar las veces que quieran para armar el rompecabezas en menos tiempo a efectos de mejorar su posición en el ranking.</li>
                    <li>En la página del concurso habrá un ranking, que mostrara los 3 (tres) primeros participantes que han armado los rompecabezas en el menor tiempo posible. Al finalizar la promoción, el primer puesto recibirá como premio un horno Serie 8 de Bosch.</li>
                    <li>Adicionalmente, se realizará un sorteo entre todas las personas participantes que se hayan registrado exitosamente. De este sorteo resultarán 3 ganadores. El primero ganará un gift card por el valor de S/.500 (Quinientos nuevos soles), el segundo por el valor de S/.300 (Trescientos nuevos soles) y el tercero por el valor de S/.200 (Doscientos nuevos soles).</li>
                    <li>El sorteo se realizará el día 12 de Noviembre a las16:00 horas en Calle Boulevard 162 Oficina 701 Edificio Metrópolis, Santiago de Surco, Lima. Fecha en la que estará presente un representante del Ministerio, a fin que de fe de la transparencia y legalidad del sorteo.</li>
                    <li>En caso de detectarse fraude, el participante será eliminado de la promoción de manera inmediata. La empresa organizadora de la promoción comercial se reserva el Derecho de eliminar a cualquier participante que defraude, altere o modifique el normal y buen funcionamiento de la dinámica y/o que recurra mediante esquemas o estrategias no convencionales a la acumulación aventajada y/o injustificada de registros en comparación de lo que se esperaría que haría de manera normal para participar, bajo el principio de que todos los participantes deben participar en igualdad de condiciones y con estricto cumplimiento de las normas de la buena fe.</li>
                    <li>Los ganadores y/o participantes autorizan a BSH ELECTRODOMÉSTICOS S.A.C. a difundir por tiempo indefinido, después de la fecha de entrega de premios del concurso, sus nombres y apellidos, edad, número de documento de identidad, así como también su imagen, con fines publicitarios, en los medios que considere, sin derecho a compensación, pago o remuneración alguna.</li>
                    <li>Los participantes que resulten ganadores del concurso reconocen que su aceptación de los presentes Términos se realiza de manera gratuita sin que proceda ni corresponda en el futuro reclamo o pretensión alguna por concepto de pago, compensación o contraprestación, indemnización, daño emergente o lucro cesante de ninguna naturaleza ya que la presente autorización es para que BSH ELECTRODOMÉSTICOS S.A.C. pueda utilizar comercialmente su información personal.</li>
                    <li>Con la participación en el concurso, los participantes aceptan que sus datos sean tratados para la verificación de su información y de las condiciones del concurso, y la entrega de premios, asimismo, para brindarle información sobre los productos y servicios de BSH ELECTRODOMÉSTICOS S.A.C. (incluyendo contacto y envío de encuestas de satisfacción) por tiempo indefinido. Su autorización para el tratamiento de sus datos personales resulta obligatoria para la ejecución de dichas actividades, y en caso de negativa, ellas no se podrán realizar.</li>
                    <li>El tratamiento y acceso a sus datos será realizado por BSH ELECTRODOMÉSTICOS S.A.C. u otra empresa del grupo, o un tercero designado por BSH ELECTRODOMÉSTICOS S.A.C., siempre conforme a los términos y a las finalidades exclusivamente señaladas en el presente consentimiento. Su información podrá ser almacenada en un banco de datos de titularidad de BSH ELECTRODOMÉSTICOS S.A.C. Para revocar su autorización para el tratamiento de su información o para cualquier consulta sobre sus datos personales puede enviar una comunicación de manera gratuita a PE-Protecciondatos@bshg.com</li>
                </ul>
            </li>
            <li>CONDICIONES:
                <ul>
                    <li>Los ganadores deberán presentar su DNI para recibir su premio.</li>
                    <li>Los premios deberán ser recogidos en la Casa Bosch ubicada en Av. El Polo 869. Urb El Derby. Surco.</li>
                    <li>Los vales de consumo son solo válidos para los productos Bosch que comercializan en la Casa Bosh ubicada en Av. El Polo 869. Urb El Derby. Surco.</li>
                    <li>Los vales son intransferibles.</li>
                    <li>Los vales no podrán ser canjeados por dinero en efectivo.</li>
                    <li>Válido para la compra de un producto cuyo precio sea mayor a S/.300. El ganador deberá cancelar la diferencia.</li>
                    <li>Si el precio del producto seleccionado por el ganador es inferior al monto del vale de consumo, el ganador no podrá requerir la diferencia en dinero efectivo.</li>
                    <li>La vigencia de los vales de consumo es desde el 13/11/2015 hasta el 14/05/2016</li>
                    <li>El vale de consumo de S/. 500 será adjudicados al  primer participante que salga sorteado, el vale de consumo S/. 300 será adjudicados al  segundo participante que salga sorteado y el vale de consumo de S/. 200 será adjudicado al  tercer  participante que salga sorteado.</li>
                </ul>
            </li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
    <p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
    <ol>
        <li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
        <li>Armar el rompecabezas en el menor tiempo posible, las imágenes de rompecabezas serán seleccionadas aleatoriamente por la empresa organizadora.</li>
        <li>Completa el formulario con los siguientes datos: Nombres, apellidos, fecha de nacimiento, teléfono, email, DNI, sexo y ciudad.</li>
        <li>Luego de completar el formulario, el usuario estará registrado en la promoción. Los participantes podrán volver a jugar las veces que quieran para armar el rompecabezas en menor tiempo a efectos de mejorar su posición en el ranking.</li>
    </ol>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
    <img src="img/premios.png" style="width: 358px;margin-top: 24px;margin-left: 78px;">
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
<script src="js/all.js"></script>
</body>
</html>