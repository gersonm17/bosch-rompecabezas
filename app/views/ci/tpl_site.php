<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>BOSCH - Hornos Serie 8</title>

    <meta name="author" content="BOSCH ">


    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Files
		<link rel="stylesheet" href="<?php echo URL::to('css/bootstrap-responsive.min.css'); ?>" media="all"/>
	<link rel="stylesheet" href="<?php echo URL::asset('/css/style.css'); ?>" media="all">-->
    <!--[if lt IE 9]><link rel="stylesheet" href="<?php echo URL::asset('/css/ie8.css'); ?>"><![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    <meta property="og:title" content="¡Participa tú también!" />
    <!-- Javascript Files

	<script src="<?php echo URL::asset('/js/jquery-1.10.2.min.js'); ?>"></script>
	<script src="<?php echo URL::asset('/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo URL::to('/js/jquery.validate.js'); ?>"></script>-->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons
	<link rel="shortcut icon" href="<?php echo URL::asset('/img/favicons/favicon.png'); ?>">
	<link rel="apple-touch-icon" href="<?php echo URL::asset('/img/favicons/apple-touch-icon.png'); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo URL::asset('/img/favicons/apple-touch-icon-72x72.png'); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo URL::asset('/img/favicons/apple-touch-icon-114x114.png'); ?>">-->
    <script type="text/javascript" src="js/jquery-1.6.min.js"></script>

</head>
<body>
<div id="fb-root"></div>
<!-- CONTENT Start -->
<?php if(isset($content)) echo $content; ?>
<!-- CONTENT End -->



<script >
    $(document).ready(initSite);

    function initSite()
    {
        $(window).resize(fitToScreen);
        fitToScreen();

    }

    function fitToScreen()
    {
        if(window.innerWidth < 768)
        {
            $('body').addClass('mobile');
            $('#mainmenu').addClass('collapse');
        } else {
            $('body').removeClass('mobile');
            $('#mainmenu').removeClass('collapse');
        }
    }

	window.fbAsyncInit = function() {
		FB.init({ appId      : '1651364221778444', cookie     : true, xfbml      : true, version    : 'v2.2'});
		FB.getLoginStatus(function(response) {/* statusChangeCallback(response);*/ });

	};
    function apply(){
            initface();
    }
	function statusChangeCallback(_response)
	{
		if (_response.status === 'connected') {
			FB.api("/me", function (response) {
					$.ajax({
						url: "<?php echo URL::to("token") ?>",
						type: 'post',
						success: function (data) {
							window.location.href = "<?php echo URL::to('facebook'); ?>";
						}

					});
				});
		}
	}

	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function testAPI() {
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
			console.log('Successful login for: ' + response.name);
			document.getElementById('status').innerHTML =
				'Thanks for logging in, ' + response.name + '!';
		});
	}


    function initface()
    {
		FB.login(function(response){
			console.log(response);
			$.ajax({
				url: "<?php echo URL::to("token") ?>",
				type: 'post',
				success: function (data) {
					window.location.href = data;
				}

			});
			// Handle the response object, like in statusChangeCallback() in our demo
			// code.
		}, {scope: 'email, public_profile, publish_actions '});


    }

	function publish1()
	{
		var message_str= 'Facebook JavaScript Graph API Tutorial';
		FB.api('/me/feed', 'post', { message: message_str}, function(response) {
			if (!response || response.error) {
				alert('Couldnt Publish Data');
			} else {
				alert("Message successfully posted to your wall");
			}
		});
	}
</script>
</body>
</html>