<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-3">
                <h1 class="text-center">Iniciar sesión</h1>
                <form action="<?php echo URL::to('users/loginclient'); ?>" method="post">
                    <p><label class="">Usuario:</label> <input type="text" name="l_username" class="form-control"></p>
                    <p><label class="">Contraseña:</label> <input type="password" name="l_password" class="form-control"></p>
                    <p class="text-center"><button type="submit" class="button">Ingresar</button></p>
                </form>
                <hr/>
            </div>
        </div>
    </div>
</div>