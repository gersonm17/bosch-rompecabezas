<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Serie 8</title>
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="http://connect.facebook.net/es_ES/all.js"></script>
    <script>
        function sharefacebook()
        {

        }
    </script>
    <script type="text/javascript">
        function submitform()
        {
            FB.init({
                appId:'1651364221778444',
                cookie:true,
                status:true,
                xfbml:true
            });
            var nu= 1;
            switch (nu)
            {
                case 1:
                    img='http://bosch-home-promociones.pe/images/POSTLINK-FACEBOOK_ROMPECABEZAS_02.png';
                    capt='¡Participa tú también!';
                    text='¡Complete el rompecabezas participando por el horno Serie 8 y los gift cards de Bosch! Juega aqui.';
                    break;
            }
            FB.ui({
                method: 'feed',
                picture: img,
                link: 'http://bosch-home-promociones.pe',
                name:'¡Increíbles premios te esperan!',
                caption: capt,
                description: text
            },function(response) {
                // if (response && !response.error_code) {
                document.myform.submit();
                //}
            });

        }
    </script>

</head>
<body>
<div class="contentpri">
<div class="banner">
    <div class="star">
        <div class="perfil" style="background: url('<?php echo (isset($me['image'])?$me['image']:'') ;?>') center;background-size: cover;"></div>
        <div class="msjstar">
            <p style="font-size:28px;font-family: 'latobold';">¡Muchas gracias por participar!</p>
            <p style="font-size:16.86px;font-family: 'latoregular';">Si quieres enterarte más de los increibles</p>
            <p style="font-size:16.86px;font-family: 'latolight';">beneficios de los nuevos hornos Serie 8<br>haz clic aquí:</p>
            <div class="coma">
                <a class="btnface3" href="http://www.bosch-home.pe/serie-8-hornos.html" target="_blank" style="font-size:13px;">Conoce más<br>de la <strong>serie 8</strong></a>
            </div>
        </div>
    </div>
</div>
<div class="game">
    <div class="formulario">
        <div style="width:100%;padding-top:30px;">
            <p style="font-size:20px;font-family: 'latolight';text-align:center;">Completa tus datos y dale clic en participar</p>
           <form id="myform" name="myform" action="<?php echo URL::to('/register'); ?>" method="post">
                <table width="740px" style="margin-left:112px;  margin-top: 30px;">
                    <tr>
                        <td>
                            <input type="text"  placeholder="Nombre*" id="name" name="name" value="<?php echo (isset($me['first_name'])?$me['first_name']:'') ?>" required>
                            <?php echo $errors->first('name', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                        <td>
                            <input type="text" placeholder="Apellidos*" id="surname" name="surname" value="<?php echo  (isset($me['last_name'])?$me['last_name']:'') ?>" required>
                            <?php echo $errors->first('surname', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                        <td>
                            <p style=" font-size: 12px;font-style: italic;color: gray;padding: 0 9px;">Fecha de<br>Nacimiento</p>
                        </td>
                        <td>
                            <?php echo $errors->first('day', '<p style="font-size:10px;">:message</p>'); ?>
                            <select id="day" name="day" required>
                                <option value="">Dia*</option>
                                <?php
                                for ($i = 1; $i <= 31; $i++) {
                                    if(isset($me['day'])){ if($me['day'] == $i){
                                        echo "<option value='$i' selected >$i</option>";
                                    }}
                                    echo "<option value='$i'>$i</option>";
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <?php echo $errors->first('month', '<p style="font-size:10px;">:message</p>'); ?>
                            <select id="month" name="month" required>
                                <option value="" >Mes*</option>
                                <option value="enero" <?php if(isset($me['month'])){ if($me['month'] == "enero"){ echo "selected";}}  ?>>Enero</option>
                                <option value="febrero" <?php if(isset($me['month'])){ if($me['month'] == "febrero"){ echo "selected";}}  ?>>Febrero</option>
                                <option value="marzo" <?php if(isset($me['month'])){ if($me['month'] == "marzo"){ echo "selected";}}  ?>>Marzo</option>
                                <option value="abril" <?php if(isset($me['month'])){ if($me['month'] == "abril"){ echo "selected";}}  ?>>Abril</option>
                                <option value="mayo" <?php if(isset($me['month'])){ if($me['month'] == "mayo"){ echo "selected";}}  ?>>Mayo</option>
                                <option value="junio" <?php if(isset($me['month'])){ if($me['month'] == "junio"){ echo "selected";}}  ?>>Junio</option>
                                <option value="julio" <?php if(isset($me['month'])){ if($me['month'] == "julio"){ echo "selected";}}  ?>>Julio</option>
                                <option value="agosto" <?php if(isset($me['month'])){ if($me['month'] == "agosto"){ echo "selected";}}  ?>>Agosto</option>
                                <option value="septiembre" <?php if(isset($me['month'])){ if($me['month'] == "septiembre"){ echo "selected";}}  ?>>Septiembre</option>
                                <option value="octubre" <?php if(isset($me['month'])){ if($me['month'] == "octubre"){ echo "selected";}}  ?>>Octubre</option>
                                <option value="noviembre" <?php if(isset($me['month'])){ if($me['month'] == "noviembre"){ echo "selected";}}  ?>>Noviembre</option>
                                <option value="diciembre" <?php if(isset($me['month'])){ if($me['month'] == "diciembre"){ echo "selected";}}  ?>>Diciembre</option>
                            </select>
                        </td>
                        <td>
                            <?php echo $errors->first('year', '<p style="font-size:10px;">:message</p>'); ?>
                            <select id="year" name="year" required>
                                <option value="" >Año*</option>
                                <?php
                                for ($i = 1924; $i <= 1997; $i++) {
                                    if(isset($me['year'])){ if($me['year'] == $i){
                                        echo "<option value='$i' selected >$i</option>";
                                    }}
                                    echo "<option value='$i'  >$i</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <input type="email" style="margin-right:14px;" placeholder="Mail*" id="email" name="email" value="<?php echo (isset($me['email'])?$me['email']:'')?>"required>
                            <?php echo $errors->first('email', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                        <td>
                            <input type="text" placeholder="DNI*" id="dni" name="dni" value="<?php echo (isset($me['dni'])?$me['dni']:'')?>" required>
                            <?php echo $errors->first('dni', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                        <td>
                            <p style=" font-size: 12px;font-style: italic;color: gray;padding: 0 9px;">Sexo(*)</p>
                        </td>
                        <?php if(isset($me['gender'])){
                            if($me['gender']=='female'){?>
                                <td>
                                    F <input type="radio" id="gender" name="gender" value="female" checked>
                                </td>
                                <td colspan="2">
                                    M <input type="radio" id="gender" name="gender"  value="male">
                                </td>
                            <?php }else{?>
                                <td>
                                    F <input type="radio" id="gender" name="gender"  value="female" >
                                </td>
                                <td colspan="2">
                                    M <input type="radio" id="gender" name="gender"  value="male" checked>
                                </td>
                            <?php }} else{ ?>
                            <td>
                                F <input type="radio" id="gender" name="gender"  value="female" >
                            </td>
                            <td colspan="2">
                                M <input type="radio" id="gender" name="gender"  value="male" checked>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" placeholder="Teléfono*" id="phone" name="phone" value="<?php echo (isset($me['phone'])?$me['phone']:'')?>" required>
                            <?php echo $errors->first('phone', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                        <td>
                            <select id="city" name="city" style="width:100%;" required>
                                <option value="">Ciudad *</option>
                                <option value="Amazonas" <?php if(isset($me['city'])){ if($me['city'] == "Amazonas"){ echo "selected";}}  ?>>Amazonas</option>
                                <option value="Ancash" <?php if(isset($me['city'])){ if($me['city'] == "Ancash"){ echo "selected";}}  ?>>Ancash</option>
                                <option value="Apurimac" <?php if(isset($me['city'])){ if($me['city'] == "Apurimac"){ echo "selected";}}  ?>>Apurimac</option>
                                <option value="Arequipa" <?php if(isset($me['city'])){ if($me['city'] == "Arequipa"){ echo "selected";}}  ?>>Arequipa</option>
                                <option value="Ayacucho" <?php if(isset($me['city'])){ if($me['city'] == "Ayacucho"){ echo "selected";}}  ?>>Ayacucho</option>
                                <option value="Cajamarca" <?php if(isset($me['city'])){ if($me['city'] == "Cajamarca"){ echo "selected";}}  ?>>Cajamarca</option>
                                <option value="Callao" <?php if(isset($me['city'])){ if($me['city'] == "Callao"){ echo "selected";}}  ?>>Callao</option>
                                <option value="Cusco" <?php if(isset($me['city'])){ if($me['city'] == "Cusco"){ echo "selected";}}  ?>>Cusco</option>
                                <option value="Huancavelica" <?php if(isset($me['city'])){ if($me['city'] == "Huancavelica"){ echo "selected";}}  ?>>Huancavelica</option>
                                <option value="Huanuco" <?php if(isset($me['city'])){ if($me['city'] == "Huanuco"){ echo "selected";}}  ?>>Huanuco</option>
                                <option value="Ica" <?php if(isset($me['city'])){ if($me['city'] == "Ica"){ echo "selected";}}  ?>>Ica</option>
                                <option value="Junin" <?php if(isset($me['city'])){ if($me['city'] == "Junin"){ echo "selected";}}  ?>>Junin</option>
                                <option value="La Libertad" <?php if(isset($me['city'])){ if($me['city'] == "La Libertad"){ echo "selected";}}  ?>>La Libertad</option>
                                <option value="Lambayeque" <?php if(isset($me['city'])){ if($me['city'] == "Lambayeque"){ echo "selected";}}  ?>>Lambayeque</option>
                                <option value="Lima - Metropolitana" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Metropolitana"){ echo "selected";}}  ?>>Lima - Metropolitana</option>
                                <option value="Lima - Cañete" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Cañete"){ echo "selected";}}  ?>>Lima - Cañete</option>
                                <option value="Lima - Barranca" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Barranca"){ echo "selected";}}  ?>>Lima - Barranca</option>
                                <option value="Lima - Chancay" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Chancay"){ echo "selected";}}  ?>>Lima - Chancay</option>
                                <option value="Lima - Huaral" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Huaral"){ echo "selected";}}  ?>>Lima - Huaral</option>
                                <option value="Lima - Huacho" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Huacho"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Lima - Huarochirí" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Huarochirí"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Lima - Cajatambo" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Cajatambo"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Lima - Oyón" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Oyón"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Lima - Yauyos" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Yauyos"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Lima - Canta" <?php if(isset($me['city'])){ if($me['city'] == "Lima - Canta"){ echo "selected";}}  ?>>Lima - Huacho</option>
                                <option value="Loreto" <?php if(isset($me['city'])){ if($me['city'] == "Loreto"){ echo "selected";}}  ?>>Loreto</option>
                                <option value="Madre de Dios" <?php if(isset($me['city'])){ if($me['city'] == "Madre de Dios"){ echo "selected";}}  ?>>Madre de Dios</option>
                                <option value="Moquegua" <?php if(isset($me['city'])){ if($me['city'] == "Moquegua"){ echo "selected";}}  ?>>Moquegua</option>
                                <option value="Pasco" <?php if(isset($me['city'])){ if($me['city'] == "Pasco"){ echo "selected";}}  ?>>Pasco</option>
                                <option value="Piura" <?php if(isset($me['city'])){ if($me['city'] == "Piura"){ echo "selected";}}  ?>>Piura</option>
                                <option value="Puno" <?php if(isset($me['city'])){ if($me['city'] == "Puno"){ echo "selected";}}  ?>>Puno</option>
                                <option value="San Martin" <?php if(isset($me['city'])){ if($me['city'] == "San Martin"){ echo "selected";}}  ?>>San Martin</option>
                                <option value="Tacna" <?php if(isset($me['city'])){ if($me['city'] == "Tacna"){ echo "selected";}}  ?>>Tacna</option>
                                <option value="Tumbes" <?php if(isset($me['city'])){ if($me['city'] == "Tumbes"){ echo "selected";}}  ?>>Tumbes</option>
                                <option value="Ucayali" <?php if(isset($me['city'])){ if($me['city'] == "Ucayali"){ echo "selected";}}  ?>>Ucayali</option>
                            </select>
                            <?php echo $errors->first('city', '<p style="font-size:10px;">:message</p>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="font-size:14px;color: #333333;">(*) Campos Obligatorios</td>
                    </tr>
                </table>
                <table class="derechos" width="740px" style="margin-left:121px;">
                    <tr>
                        <td style="padding-right: 16px;">
                            <input type="checkbox" checked name="data_processing" value="si">
                        </td>
                        <td style="padding-right: 16px;">
                            Autorizo el tratamiento de mis datos personales. Conozco y estoy de acuerdo con la Política de Protección de los Datos y Seguridad de la información de Bosch Electrodomésticos*
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding:20px 0;"><a href="http://www.bosch-home.pe/aviso-legal.html" target="_blank">> Política de Protección de los Datos y Seguridad de la información de Bosch.</a></td>
                    </tr>
                </table>
                <div style="text-align:center;margin-top:20px;">
                    <button  class="btnface" style="outline: none;border: none;padding-left: 30px;padding-right: 30px;font-family:'latoregular';font-size:16px;">CONTINUAR</button>
                </div>
            </form>
        </div>
    </div>
    <div class="buttons">
        <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
            <img src="img/terminos.png">
        </a>
        <a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
            <img src="img/participar.png">
        </a>
        <a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
            <img src="img/premio.png">
        </a>
    </div>
    <div class="rd">
        <p>RD 25145267 - GDAIA</p>
    </div>
</div>
</div>
<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll">
        <ul class="letters" style="font-size: 13px;">
            <li>Duración: 3 semanas</li>
            <li>Fecha de Inicio: 31 de Agosto</li>
            <li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
            <li>Fecha de sorteo: 22 de Setiembre</li>
            <li>Publicación de ganadores: 23 de Setiembre</li>
            <li>Premios
                <ul>
                    <li>3 libros "Proyecto + Gourmet"</li>
                </ul>
            </li>
            <li>Descripción del mecanismo y las condiciones del sorteo:
                <ul>
                    <li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
                    <li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
                </ul>
            </li>
            <li>
                Cómo participar
                <ul>
                    <li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
                    <li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
                    <li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
                        <ul>
                            <li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
                        </ul>
                    </li>
                    <li>
                        Cómo tener más opciones de jugar:
                        <ul>
                            <li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                            <li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
            <li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
            <li>El concurso es válido únicamente para Lima.</li>
            <li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
            <li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
            <li>Fecha de canje: 23 de Setiembre</li>
            <li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
            <li>Promoción solo válida para Lima.</li>
            <li>Requisito para hacer efectivo el premio DNI original y copia.</li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="comopar" class="reveal-modal"  style="font-family: 'latolight';font-size: 14px;">
    <p class="letterm" style="font-weight: blid;font-family: 'latoregular';text-align:center;">COMO PARTICIPAR</p>
    <p class="letters" style="clior: #666666;text-align:center;padding-top:15px;padding-bottom:35px;">Esto es muy sencillo. Sólo sigue los siguientes pasos
        <br>
        y ya estarás participando.
    </p>
    <ol class="letters" style="padding:0 25px; color:black;font-weight: bold;">
        <li style="padding-bottom:10px;">Para revelar un electrodoméstico, dale click a la espalda de la carta.</li>
        <li style="padding-bottom:10px;">Acepta la conexión a Facebook para compartirlo en tu muro.</li>
        <li style="padding-bottom:10px;">Finaliza completando tus datos y dando clic en PARTICIPAR.</li>
        <li style="padding-bottom:10px;">Mientras más cantidad de amigos invites y más veces juegues, tienes más opciones de ganar.</li>
    </ol>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>

    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
<script src="js/all.js"></script>
</body>
</html>