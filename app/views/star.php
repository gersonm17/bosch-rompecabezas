<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Serie 8</title>
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body ">
<div class="contentpri">
    <div class="banner">
        <div class="star">
            <div class="perfil" style="background: url('<?php echo (isset($me['image'])?$me['image']:'') ;?>') center;background-size: cover;"></div>
            <div id="start2" class="msjstar">
                <p class="bol">¡Hola, <?php echo (isset($me['first_name'])?$me['first_name']:'') ;?>!</p>
                <p class="lig">¿Preparado para hacer el reto Serie 8?<br>¡Memoriza el rompecabezas y ármalo en el menor<br> tiempo posible!</p>
                <p class="re">Cuando estes listo, dale click al botón de JUGAR</p>
            </div>
            <div id="games2" class="msjstar" style="top:142px;display: none">
                <p style="font-size:28px;font-family: 'latobold';">¡Vamos <?php echo (isset($me['first_name'])?$me['first_name']:'') ;?>!</p>
                <p style="font-size:16.86px;font-family: 'latolight';">¡Arma el rompecabezas en el menor tiempo<br> posible y completa el reto Serie 8!</p>
            </div>
        </div>
    </div>
    <div class="game">
        <div class="rompe">
            <div class="timer">
                <p style="font-size:20px;padding-top: 9px;">Tiempo recorrido</p>
                <div id="cronometro">
                    <div id="reloj" style="font-size:30px;">
                        00:00:00
                    </div>
                </div>
            </div>
            <div id="start" >
            <div style="width:467px;height:471px;float:left;">
                <img src="img/horno.png">
                <div class="centralgame">
                    <div><img src="<?php echo (isset($imagen['image'])?$imagen['image']:'') ;?>" alt="" width="350" height="263" /></div>
                </div>
            </div>
            <div  style="width:257.5px;height:470px;float:left;text-align:center;">
                <a onclick="jugar('<?php echo (isset($imagen['image'])?$imagen['image']:'') ;?>');" class="btnface2" style="font-size:21.42px;">Jugar</a>
            </div>
            </div>
            <div style="display: none;" id="games">
                <div class="central">
                    <img src="img/horno.png">
                    <div id="content" class="centralgame">
                        <canvas id="canvas"></canvas>
                    </div>
                </div>
                <div  class="benefi">
                    <?php if($imagen['type']=='4D HotAir'){?>
                    <img src="img/4dair.png" style="margin-top:80px;">
                    <?php } elseif($imagen['type']=='PerfectRoast'){?>
                        <img src="img/roast.png" style="margin-top:80px;">
                    <?php } else{?>
                        <img src="img/bake.png" style="margin-top:80px;">
                    <?php } ?>
                </div></div>
        </div>
        <div class="buttons">
            <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
                <img src="img/terminos.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
                <img src="img/participar.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
                <img src="img/premio.png">
            </a>
        </div>
        <div class="rd">
            <p>RD 25145267 - GDAIA</p>
        </div>
    </div>
</div>
<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll">
        <ul class="letters" style="font-size: 13px;">
            <li>Duración: 3 semanas</li>
            <li>Fecha de Inicio: 31 de Agosto</li>
            <li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
            <li>Fecha de sorteo: 22 de Setiembre</li>
            <li>Publicación de ganadores: 23 de Setiembre</li>
            <li>Premios
                <ul>
                    <li>3 libros "Proyecto + Gourmet"</li>
                </ul>
            </li>
            <li>Descripción del mecanismo y las condiciones del sorteo:
                <ul>
                    <li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
                    <li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
                </ul>
            </li>
            <li>
                Cómo participar
                <ul>
                    <li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
                    <li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
                    <li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
                        <ul>
                            <li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
                        </ul>
                    </li>
                    <li>
                        Cómo tener más opciones de jugar:
                        <ul>
                            <li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                            <li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
            <li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
            <li>El concurso es válido únicamente para Lima.</li>
            <li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
            <li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
            <li>Fecha de canje: 23 de Setiembre</li>
            <li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
            <li>Promoción solo válida para Lima.</li>
            <li>Requisito para hacer efectivo el premio DNI original y copia.</li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
    <div class="cont-scroll">
        <p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
        <p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
        <ol>
            <li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
            <li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
            <li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
        </ol>
        <ul>
            <li>
                Cómo tener más opciones de jugar:<br><br>
                <ul>
                    <li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                    <li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
                </ul>
            </li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
    <img src="img/premios.png" style="width: 500px;margin-top: 50px;margin-left: 20px;">
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<script type="text/javascript" src="js/rompe.js"></script>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
<script src="js/all.js"></script>
<script src="js/mootools.v1.00.js" type="text/javascript"></script>
<script src="js/puzzle-not-compressed.js" type="text/javascript"></script>
</body>
</html>