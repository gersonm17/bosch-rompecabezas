<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Verdad o Mito</title>
	<link rel="stylesheet" type="text/css" href="css/reveal.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="content">
		<div class="banner">
			<div class="star">
				<div class="perfil">
					<div class="profile-photo-circular">
						<img src="<?php echo (isset($me['image'])?$me['image']:'') ;?>">
					</div>
				</div>
				<div class="msjstar">
					<p style="font-size:28px;font-family: 'latobold';">¡Hola, <?php echo (isset($me['first_name'])?$me['first_name']:'') ;?>!</p>
					<p style="font-size:20px;font-family: 'latoregular';">Demuestra tu capacidad<br><span style="font-family: 'latolight';">cuanto sabes de hornear.</span></p>
				</div>
			</div>
		</div>
		<div class="game">
			<div class="pregunta">
				<div id="<?php echo(isset($question[0]['id'])?$question[0]['id']:'');?>" class="izq">
					<p class="ques"><?php echo (isset($question[0]['question'])?$question[0]['question']:'') ;?> <span>¿Verdad o mito?</span></p>
					<div class="int">
                        <input type="hidden" id="rpta0" value="<?php echo(isset($question[0]['rpta'])?$question[0]['rpta']:'');?>">
                        <?php if($question[0]['rpta']=='verdad'){ ?>
						<div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'correcto':'');?>" style="display: none" class="resverdad">
							<p>CORRECTO</p>
						</div>
						<div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'incorrecto':'');?>" style="display: none" class="resmito">
							<p>INCORRECTO</p>
						</div>
                            <img src="images/asta.png" id="<?php echo (isset($question[0]['id'])?$question[0]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[0]['id'])?$question[0]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[0]['id'])?"'".$question[0]['id'].'correcto'."'":'');?>,<?php echo (isset($question[0]['id'])?"'".$question[0]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[0]['id'])?"'".$question[0]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[0]['id'])?"'".$question[0]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }else{?>
                            <div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'correcto':'');?>" style="display: none" class="resmito">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'incorrecto':'');?>" style="display: none" class="resverdad">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[0]['id'])?$question[0]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[0]['id'])?$question[0]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[0]['id'])?"'".$question[0]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[0]['id'])?"'".$question[0]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[0]['id'])?"'".$question[0]['id'].'correcto'."'":'');?>,<?php echo (isset($question[0]['id'])?"'".$question[0]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }?>

						<div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'explica':'');?>"   style="display: none">
                            <div class="explica">
                            <div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'msjincorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[0]['msjincorrect'])?$question[0]['msjincorrect']:'');?></p>
                            </div>
                            <div id="<?php echo(isset($question[0]['id'])?$question[0]['id'].'msjcorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[0]['msjcorrect'])?$question[0]['msjcorrect']:'');?></p>
                            </div>
                                </div>

						<div style="position:absolute;bottom:0;text-align:center;width:100%;">
							<a onclick="pasar(<?php echo(isset($question[0]['id'])?$question[0]['id']:'');?>,<?php echo(isset($question[1]['id'])?$question[1]['id']:'');?>)" class="btnface" style="padding-left: 30px;padding-right: 30px;"> Siguiente</a>
						</div>
                        </div>
					</div>
				</div>
                <div id="<?php echo(isset($question[1]['id'])?$question[1]['id']:'');?>" style="display: none" class="izq">
                    <p class="ques"><?php echo (isset($question[1]['question'])?$question[1]['question']:'') ;?> <span>¿Verdad o mito?</span></p>
                    <div class="int">
                        <input type="hidden" id="rpta1" value="<?php echo(isset($question[1]['rpta'])?$question[1]['rpta']:'');?>">
                        <?php if($question[0]['rpta']=='verdad'){ ?>
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'correcto':'');?>" style="display: none" class="resverdad">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'incorrecto':'');?>" style="display: none" class="resmito">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[1]['id'])?$question[1]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[1]['id'])?$question[1]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[1]['id'])?"'".$question[1]['id'].'correcto'."'":'');?>,<?php echo (isset($question[1]['id'])?"'".$question[1]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[1]['id'])?"'".$question[1]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[1]['id'])?"'".$question[1]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }else{?>
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'correcto':'');?>" style="display: none" class="resmito">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'incorrecto':'');?>" style="display: none" class="resverdad">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[1]['id'])?$question[1]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[1]['id'])?$question[1]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[1]['id'])?"'".$question[1]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[1]['id'])?"'".$question[1]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[1]['id'])?"'".$question[1]['id'].'correcto'."'":'');?>,<?php echo (isset($question[1]['id'])?"'".$question[1]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }?>

                        <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'explica':'');?>"  style="display: none">
                            <div class="explica">
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'msjincorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[1]['msjincorrect'])?$question[1]['msjincorrect']:'');?></p>
                            </div>
                            <div id="<?php echo(isset($question[1]['id'])?$question[1]['id'].'msjcorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[1]['msjcorrect'])?$question[1]['msjcorrect']:'');?></p>
                            </div>
                            </div>

                        <div style="position:absolute;bottom:0;text-align:center;width:100%;">
                            <a onclick="pasar(<?php echo(isset($question[1]['id'])?$question[1]['id']:'');?>,<?php echo(isset($question[2]['id'])?$question[2]['id']:'');?>)" class="btnface" style="padding-left: 30px;padding-right: 30px;"> Siguiente</a>
                        </div>
                        </div>
                    </div>
                </div>
                <div id="<?php echo(isset($question[2]['id'])?$question[2]['id']:'');?>" class="izq" style="display: none">
                    <p class="ques"><?php echo (isset($question[2]['question'])?$question[2]['question']:'') ;?> <span>¿Verdad o mito?</span></p>
                    <div class="int">
                        <input type="hidden" id="rpta2" value="<?php echo(isset($question[2]['rpta'])?$question[2]['rpta']:'');?>">
                        <?php if($question[2]['rpta']=='verdad'){ ?>
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'correcto':'');?>" style="display: none" class="resverdad">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'incorrecto':'');?>" style="display: none" class="resmito">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[2]['id'])?$question[2]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[2]['id'])?$question[2]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[2]['id'])?"'".$question[2]['id'].'correcto'."'":'');?>,<?php echo (isset($question[2]['id'])?"'".$question[2]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[2]['id'])?"'".$question[2]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[2]['id'])?"'".$question[2]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }else{?>
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'correcto':'');?>" style="display: none" class="resmito">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'incorrecto':'');?>" style="display: none" class="resverdad">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[2]['id'])?$question[2]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[2]['id'])?$question[2]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[2]['id'])?"'".$question[2]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[2]['id'])?"'".$question[2]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[2]['id'])?"'".$question[2]['id'].'correcto'."'":'');?>,<?php echo (isset($question[2]['id'])?"'".$question[2]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }?>

                        <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'explica':'');?>"   style="display: none">
                            <div class="explica">
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'msjincorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[2]['msjincorrect'])?$question[2]['msjincorrect']:'');?></p>
                            </div>
                            <div id="<?php echo(isset($question[2]['id'])?$question[2]['id'].'msjcorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[2]['msjcorrect'])?$question[2]['msjcorrect']:'');?></p>
                            </div>
                        </div>
                        <div style="position:absolute;bottom:0;text-align:center;width:100%;">
                            <a onclick="pasar(<?php echo(isset($question[2]['id'])?$question[2]['id']:'');?>,<?php echo(isset($question[3]['id'])?$question[3]['id']:'');?>)" class="btnface" style="padding-left: 30px;padding-right: 30px;"> Siguiente</a>
                        </div>
                        </div>
                    </div>
                </div>
                <div id="<?php echo(isset($question[3]['id'])?$question[3]['id']:'');?>" class="izq" style="display: none">
                    <p class="ques"><?php echo (isset($question[3]['question'])?$question[3]['question']:'') ;?> <span>¿Verdad o mito?</span></p>
                    <div class="int">
                        <input type="hidden" id="rpta3" value="<?php echo(isset($question[3]['rpta'])?$question[3]['rpta']:'');?>">
                        <?php if($question[3]['rpta']=='verdad'){ ?>
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'correcto':'');?>" style="display: none" class="resverdad">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'incorrecto':'');?>" style="display: none" class="resmito">
                                <p>INCORRECTO</p>
                            </div>
                            <img src="images/asta.png" id="<?php echo (isset($question[3]['id'])?$question[3]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[3]['id'])?$question[3]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[3]['id'])?"'".$question[3]['id'].'correcto'."'":'');?>,<?php echo (isset($question[3]['id'])?"'".$question[3]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[3]['id'])?"'".$question[3]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[3]['id'])?"'".$question[3]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }else{?>
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'correcto':'');?>" style="display: none" class="resmito">
                                <p>CORRECTO</p>
                            </div>
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'incorrecto':'');?>" style="display: none" class="resverdad">
                                <p>INCORRECTO</p>
                            </div>

                            <img src="images/asta.png" id="<?php echo (isset($question[3]['id'])?$question[3]['id'].'averdad':'');?>" style="position: absolute;top: 46px;left: 184px;display: none">
                            <img src="images/asta.png" id="<?php echo (isset($question[3]['id'])?$question[3]['id'].'amito':'');?>" style="position: absolute;top: 46px;left: 394px;display: none">
                            <div onclick="respuestaclick(<?php echo(isset($question[3]['id'])?"'".$question[3]['id'].'incorrecto'."'":'');?>,<?php echo (isset($question[3]['id'])?"'".$question[3]['id'].'averdad'."'":'');?>,'verdad')" class="verdad">
                                <p >VERDAD</p>
                            </div>
                            <div onclick="respuestaclick(<?php echo(isset($question[3]['id'])?"'".$question[3]['id'].'correcto'."'":'');?>,<?php echo (isset($question[3]['id'])?"'".$question[3]['id'].'amito'."'":'');?>,'mito')" class="mito">
                                <p >MITO</p>
                            </div>
                        <?php }?>
                        <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'explica':'');?>"  style="display: none">
                        <div class="explica" >
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'msjincorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[3]['msjincorrect'])?$question[3]['msjincorrect']:'');?></p>
                            </div>
                            <div id="<?php echo(isset($question[3]['id'])?$question[3]['id'].'msjcorrecto':'');?>" style="display: none">
                                <p><?php echo(isset($question[3]['msjcorrect'])?$question[3]['msjcorrect']:'');?></p>
                            </div>
                        </div>
                        <div style="position:absolute;bottom:0;text-align:center;width:100%;">
                            <a onclick="finalizar()" class="btnface" style="padding-left: 30px;padding-right: 30px;"> Finalizar</a>
                        </div>
                        </div>
                    </div>
                </div>

				<div class="der">
					<p style="color:black;font-size:15px;margin-top:32px;">Tiempo</p>
					<div id="cronometro">
					  <div id="reloj">
						  00:00:00
						</div>
					
					</div>
					<img id="image0" src="<?php echo(isset($question[0]['image'])?$question[0]['image']:'');?>" style="width:310px; display: block;">
                    <img id="image1" src="<?php echo(isset($question[1]['image'])?$question[1]['image']:'');?>" style="width:310px; display: none;">
                    <img id="image2" src="<?php echo(isset($question[2]['image'])?$question[2]['image']:'');?>" style="width:310px; display: none;">
                    <img id="image3" src="<?php echo(isset($question[3]['image'])?$question[3]['image']:'');?>" style="width:310px; display: none;">
					<div class="cajaguia">
						<ul id="rpta">

						</ul>
					</div>
				</div>
			</div>
			<div class="buttons">
				<a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
					<img src="images/terminos.png">
				</a>
				<a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
					<img src="images/participar.png">
				</a>
				<a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
					<img src="images/premio.png">
				</a>
			</div>
			<div class="rd">
				<p>RD 25145267 - GDAIA</p>
			</div>
		</div>
	</div>
	<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
		<p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
		<div class="cont-scroll">
			<ul class="letters" style="font-size: 13px;">
					<li>Duración: 3 semanas</li>
					<li>Fecha de Inicio: 31 de Agosto</li>
					<li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
					<li>Fecha de sorteo: 22 de Setiembre</li>
					<li>Publicación de ganadores: 23 de Setiembre</li>
					<li>Premios
						<ul>
							<li>3 libros "Proyecto + Gourmet"</li>
						</ul>
					</li>
					<li>Descripción del mecanismo y las condiciones del sorteo:
						<ul>
							<li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
							<li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
						</ul>
					</li>
					<li>
						Cómo participar
						<ul>
							<li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
							<li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
							<li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
								<ul>
									<li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
								</ul>
							</li>
							<li>
								Cómo tener más opciones de jugar:
								<ul>
									<li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
									<li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
								</ul>
							</li>
						</ul>
					</li>
					<li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
					<li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
					<li>El concurso es válido únicamente para Lima.</li>
					<li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
					<li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
					<li>Fecha de canje: 23 de Setiembre</li>
					<li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
					<li>Promoción solo válida para Lima.</li>
					<li>Requisito para hacer efectivo el premio DNI original y copia.</li>
				</ul>
		</div>
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
		<div class="cont-scroll">
			<p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
			<p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
			<ol>
				<li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
				<li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
				<li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
			</ol>
			<ul>
				<li>
					Cómo tener más opciones de jugar:<br><br>
					<ul>
						<li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
						<li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
					</ul>
				</li>
			</ul>
		</div>
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<div id="premiosof" class="reveal-modal">
		<p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
		<img src="images/premios.png" style="width: 500px;margin-top: 50px;margin-left: 20px;">
		<a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
	</div>
	<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
	<script src="js/jquery.reveal.js"></script>
	<script src="js/all.js"></script>
</body>
</html>