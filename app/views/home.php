<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Serie 8</title>
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="contentpri">
    <div class="banner">
        <div class="smsbienvenida">
            <p style="padding-top:85px;padding-bottom:25px;">¡Bienvenido al Reto Serie 8!</p>
            <p class="mensaje">Arma los rompecabezas y participa del sorteo por uno de nuestros fabulosos premios.</p>
            <a id='token' onclick="apply()" class="btnface"><span class="icon-facebook3" style="position: absolute;left: 30px;font-size: 19px;bottom: 8px;"></span>PARA PARTICIPAR CONÉCTATE CON FACEBOOK</a><br>
        </div>
    </div>
    <div class="game">
        <div class="como">
            <p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-top:50px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
            <p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
            <ol>
                <li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
                <li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
                <li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
            </ol>
            <ul>
                <li>
                    Cómo tener más opciones de jugar:<br><br>
                    <ul>
                        <li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                        <li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="buttons">
            <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
                <img src="img/terminos.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
                <img src="img/participar.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
                <img src="img/premio.png">
            </a>
        </div>
        <div class="rd">
            <p>RD 25145267 - GDAIA</p>
        </div>
    </div>
</div>
<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll">
        <ul class="letters" style="font-size: 13px;">
            <li>Duración: 3 semanas</li>
            <li>Fecha de Inicio: 31 de Agosto</li>
            <li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
            <li>Fecha de sorteo: 22 de Setiembre</li>
            <li>Publicación de ganadores: 23 de Setiembre</li>
            <li>Premios
                <ul>
                    <li>3 libros "Proyecto + Gourmet"</li>
                </ul>
            </li>
            <li>Descripción del mecanismo y las condiciones del sorteo:
                <ul>
                    <li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
                    <li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
                </ul>
            </li>
            <li>
                Cómo participar
                <ul>
                    <li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
                    <li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
                    <li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
                        <ul>
                            <li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
                        </ul>
                    </li>
                    <li>
                        Cómo tener más opciones de jugar:
                        <ul>
                            <li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                            <li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
            <li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
            <li>El concurso es válido únicamente para Lima.</li>
            <li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
            <li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
            <li>Fecha de canje: 23 de Setiembre</li>
            <li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
            <li>Promoción solo válida para Lima.</li>
            <li>Requisito para hacer efectivo el premio DNI original y copia.</li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
    <div class="cont-scroll">
        <p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
        <p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
        <ol>
            <li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
            <li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
            <li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
        </ol>
        <ul>
            <li>
                Cómo tener más opciones de jugar:<br><br>
                <ul>
                    <li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                    <li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
                </ul>
            </li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
    <img src="img/premios.png" style="width: 500px;margin-top: 50px;margin-left: 20px;">
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
</body>
</html>