<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Serie 8</title>
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick/slick-theme.css"/>
</head>
<body>
<div class="contentpri">
    <div class="banner" >
        <div class="star">
            <div class="perfil" style="background: url('<?php echo (isset($me['image'])?$me['image']:'') ;?>') center;background-size: cover;"></div>
            <div class="msjstar" style="top:132px;">
                <p style="font-size:28px;font-family: 'latobold';">¡Muchas gracias por participar!</p>
                <a href="iniciar" class="btnface2" style="font-size: 18px;margin-top: 15px;">Mejorar mi tiempo</a>
            </div>
        </div>
    </div>
    <div class="game">
        <div style="text-align:center;">
            <p style="font-size:28px;font-family: 'latobold';margin-top:57px;margin-bottom:30px;">Top Ranking:</p>
            <div  class="cont-scroll">
                <img src="img/blanco.png" style="position: absolute;bottom: 123px;margin-left: -27px;z-index: 999;">
                <?php $count=0; foreach($ranking as $rank){ $count=$count+1;  ?>

                <div class="people">
                    <div class="perfilra" style="background: url('<?php echo  'https://graph.facebook.com/' .$rank->fid.'/picture?type=large';?>') center;background-size: cover;"></div>
                    <div class="name">
                        <div style="position:absolute;">
                            <p style="font-family: 'latobold';font-size:20px;"><?php echo (isset($rank->name)?$rank->name:'') ;?></p>
                            <p style="font-family: 'latolight';">Tiempo recorrido:</p>
                            <p style="font-family: 'latolight';color:red;"><?php echo (isset($rank->time)?$rank->time:'') ;?></p>
                        </div>
                        <div class="posi">
                            <p style="margin-top: 35px;font-family:'latoregular';"><p><?php echo $count ?></p></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="buttons">
            <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade" style="margin-left:10px;">
                <img src="img/terminos.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="comopar" data-animation="fade"  style="margin-left: 480px;margin-right: 9px;">
                <img src="img/participar.png">
            </a>
            <a href="#" class="big-link" data-reveal-id="premiosof" data-animation="fade" >
                <img src="img/premio.png">
            </a>
        </div>
        <div class="rd">
            <p>RD 25145267 - GDAIA</p>
        </div>
    </div>
</div>
<div id="terminosyco" class="reveal-modal" style="font-family: 'latolight';font-size: 17px;">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll">
        <ul class="letters" style="font-size: 13px;">
            <li>Duración: 3 semanas</li>
            <li>Fecha de Inicio: 31 de Agosto</li>
            <li>Fecha de finalización: 21 de Septiembre a las 0:00hrs.</li>
            <li>Fecha de sorteo: 22 de Setiembre</li>
            <li>Publicación de ganadores: 23 de Setiembre</li>
            <li>Premios
                <ul>
                    <li>3 libros "Proyecto + Gourmet"</li>
                </ul>
            </li>
            <li>Descripción del mecanismo y las condiciones del sorteo:
                <ul>
                    <li>El 1er, 2do y 3er puesto del ranking ganarán los libros “Proyecto + Gourmet” de manera automática</li>
                    <li>Se hará un sorteo de 10 libros entre las personas que se hayan registrado con éxito y hayan respondido todas las preguntas correctamente.</li>
                </ul>
            </li>
            <li>
                Cómo participar
                <ul>
                    <li> Los concursantes deberán aceptar la conexión  a Facebook, dar click en el botón Start para iniciar el juego de trivia.</li>
                    <li>Los usuarios deben responder una serie de 4 preguntas seleccionando “Verdad” o “Mito” en el menor tiempo posible</li>
                    <li>El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
                        <ul>
                            <li>Aceptar conexión con Facebook, participar del juego de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
                        </ul>
                    </li>
                    <li>
                        Cómo tener más opciones de jugar:
                        <ul>
                            <li>El usuario al ser la primera vez que participa genera 3 opciones de juego. Una vez cubierta las opciones contará con una opción extra si invita a sus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                            <li>La segunda vez que ingresa, sólo puede participar una vez al día. Sin embargo, si invita a sus amigos durante el mismo día, recibe una opción más para participar. Es decir, puede jugar hasta dos veces por día.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>El sorteo de los diez ganadores se realizará entre todas las personas que se hayan registrado exitosamente y a la vez hayan respondido todas las preguntas correctamente.</li>
            <li>Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
            <li>El concurso es válido únicamente para Lima.</li>
            <li>Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
            <li>Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
            <li>Fecha de canje: 23 de Setiembre</li>
            <li>Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
            <li>Promoción solo válida para Lima.</li>
            <li>Requisito para hacer efectivo el premio DNI original y copia.</li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="comopar" class="reveal-modal" style="font-family: 'latolight';font-size: 14px;">
    <div class="cont-scroll">
        <p style="text-align:center;font-family: 'latoregular';font-size:20px;padding-bottom:20px;">CÓMO PARTICIPAR</p>
        <p style="text-align:center;color:#b2b2b2;">Esto es muy sencillo. Sólo sigue los siguientes pasos<br> y ya estarás participando.</p>
        <ol>
            <li>Aceptar la conexión a Facebook, seleecionar el botón Start para iniciar el juego.</li>
            <li>Responder las 4 preguntas seleccionando “Verdad  o Mito” en el menor tiempo posible.</li>
            <li>Llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones. </li>
        </ol>
        <ul>
            <li>
                Cómo tener más opciones de jugar:<br><br>
                <ul>
                    <li>La primera vez que se ingresa, se generan 3 opciones de juego. Adicionalmente, se otorgará una opción más si invitas tus amigos. Si no se completan las 3 opciones de juego en una sola sesión se considerará como si el participante hubiera completado todas las opciones.</li>
                    <li>Sólo se puede participar una vez al día. Sin embargo, si invitas a tus amigos durante el mismo día, se otorga una opción más para participar.</li>
                </ul>
            </li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
    <img src="img/premios.png" style="width: 500px;margin-top: 50px;margin-left: 20px;">
    <a class="close-reveal-modal">Cerrar <img src="img/close.png"></a>
</div>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
<script src="js/all.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="css/slick/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.slideno').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: ("<img src='img/derecha.png' style='  position: absolute;top: calc(50% - 37px);left: -60px;cursor:pointer;'>"),
            nextArrow: ("<img src='img/izquierda.png' style='  position: absolute;top: calc(50% - 37px);right: -60px;cursor:pointer;'>")
        });
    });
</script>
</body>
</html>