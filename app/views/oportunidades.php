<!DOCTYPE html>
<html>
<head>
    <title>BOSCH - Hornos Serie 8</title>
    <meta charset="UTF-8">
    <meta property="og:image" content="http://bosch-home-promociones.pe/images/auto.jpg" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="500" />
    <meta property="og:description" content="Juega, invita a tus amigos y gana opciones para el sorteo de una de nuestras Gift Cards." />
    <link rel="stylesheet" type="text/css" href="css/reveal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="content">
    <div class="uppart">
        <div class="tetx-gracias">
            <p>Gracias por participar, <span><?php echo (isset($me['first_name'])?$me['first_name']:'') ?></span></p>
        </div>
        <div class="btnpremios gracias" onclick="window.open('http://www.bosch-home.pe/serie-8-hornos.html','new_window');">
            <p style="margin-top:50px;">Conoce más de la <span>Serie 8 aquí</span></p>
        </div>
        <img src="images/flecha-izquierda.png" class="arrow">
    </div>
    <div class="downpart">
        <img src="images/hornolimpio.jpg" style="width:100%;">
        <div class="opciones-ganar">
            <p class="title">¡Ahora tienes más opciones de ganar!</p>
            <p class="desc">¡Gracias por invitar a tus amigos!</p>
            <img src="images/check.png" style="margin-top: 60px;">
        </div>
    </div>
    <div class="terminos">
        <a href="http://www.bosch-home.pe/" ><img src="images/regresar.png" style="left:4px;"></a>
         <a href="#" class="big-link" data-reveal-id="terminosyco" data-animation="fade"><img src="images/terminos.png" style="left:408px;"></a>
        <a hhref="#" class="big-link" data-reveal-id="comopar" data-animation="fade"><img src="images/comoparticipar.png" style="right:156px;"></a>
        <a hhref="#" class="big-link" data-reveal-id="premiosof" data-animation="fade"><img src="images/premios.png" style="right:4px;"></a>
        <a hhref="#" class="big-link" data-reveal-id="gana" id="ganador" data-animation="fade"></a>
    </div>
    <div style="margin-bottom:20px;text-align:center; font-size:10px;">
        <p>Promoción válida del 06/07/2015 al 23/07/2015. No aplica stock. RD° N° 2923-2015-ONAGI-DGAE-DA. Sorteo: 23/07/15.</p>
    </div>
</div>
<div id="terminosyco" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">TÉRMINOS Y CONDICIONES</p>
    <div class="cont-scroll">
        <ul class="letters" style="margin: 0 40px;padding-top: 40px;  font-size: 13px;">
            <li style="padding-bottom:10px;">Duración: 17 días</li>
            <li style="padding-bottom:10px;">Fecha de Inicio: 06/07/2015.</li>
            <li style="padding-bottom:10px;">Fecha de finalización: 23/07/2015. (00 Hrs)</li>
            <li style="padding-bottom:10px;">Fecha de sorteo: 23/07/2015.</li>
            <li style="padding-bottom:10px;">N° de premios: 03 premios:
                <ul>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">1er premio: 1 Gift card s/. 500.00</li>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">1er premio: 1 Gift card s/. 300.00</li>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">1er premio: 1 Gift card s/. 200.00</li>
                </ul>
            </li>
            <li style="padding-bottom:10px;">Descripción del mecanismo y las condiciones del sorteo:
                <ul>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">Se hará 1 sorteo entre todas las personas que se hayan registrado con éxito, y se contarán las opciones que cada uno ha generado.</li>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">La aplicación consistirá en un concurso en el que se sortearán (01) 1 Gift card S/.500.00, (01) 1 Gift card S/.300.00 y (01) 1 Gift card S/.200.00.</li>
                    <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">Para participar, los concursantes deberán aceptar la conexión a Facebook, responder la trivia de 4 preguntas y registrarse correctamente.</li>
                    <li style="padding-bottom:10px;margin-left: 15px;">El registro para el concurso se realizará mediante la página web de Bosch donde los participantes tendrán que:
                        <ol>
                            <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">Aceptar conexión con Facebook, participar de la trivia, llenar el formulario ingresando los datos solicitados, aceptar los términos y condiciones y apretar el botón Registrarse.</li>
                        </ol>
                    </li>
                    <li style="padding-bottom:10px;margin-left: 15px;">Cómo acumular opciones:
                        <ul>
                            <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">Por registro: Sólo puede registrarse una vez.</li>
                            <li style="padding-top:10px; padding-bottom:10px;  margin-left: 15px;">Invitando amigos a jugar: generará 5 opciones más.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li style="padding-bottom:10px;">El sorteo de los tres ganadores se realizará entre todas las personas que se hayan registrado exitosamente, tomando en cuenta el número de opciones que han generado.</li>
            <li style="padding-bottom:10px;">Los seleccionados serán notificados vía correo electrónico y/o telefónico.</li>
            <li style="padding-bottom:10px;">El premio del concurso es válido únicamente para Lima.</li>
            <li style="padding-bottom:10px;">Si se detecta algún tipo de intento de fraude (spam, hackeo,etc), los concursantes quedarán automáticamente fuera del sorteo.</li>
            <li style="padding-bottom:10px;">Los ganadores al momento de recibir su premio deberán llevar su DNI.</li>
            <li style="padding-bottom:10px;">Fecha de canje: Desde el 24/07/2015 al 25/01/2015</li>
            <li style="padding-bottom:10px;">Lugar de canje: Casa Bosch. Av. El Polo 869 - Santiago de Surco, Lima.</li>
            <li style="padding-bottom:10px;">Promoción solo válida para Lima.</li>
            <li style="padding-bottom:10px;">Requisito para hacer efectivo el premio DNI original y copia.</li>
        </ul>
    </div>
    <a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
</div>
<div id="comopar" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">COMO PARTICIPAR</p>
    <p class="letters" style="color: #666666;text-align:center;padding-top:15px;padding-bottom:35px;">Esto es muy sencillo. Sólo sigue los siguientes pasos
        <br>
        y ya estarás participando.
    </p>
    <ol class="letters" style="padding:0 25px; color:black;font-weight: bold;">
        <li style="padding-bottom:10px;">Acepta la conexión a Facebook para poder jugar la trivia.</li>
        <li style="padding-bottom:10px;">Responde las preguntas referentes al horneado y cocina, deberás registrarte llenando un formulario con los datos solicitados. Así obtienes una opción de ganar.</li>
        <li style="padding-bottom:10px;">Debes aceptar lo términos y condiciones.</li>
        <li style="padding-bottom:10px;">Invita a tus amigos y obtén 5 opciones adicionales en el sorteo.</li>
    </ol>
    <a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
</div>
<div id="premiosof" class="reveal-modal">
    <p class="letterm" style="font-weight: bold;font-family: 'latoregular';text-align:center;">PREMIOS</p>
    <img src="images/premios.jpg" style="width:100%;">
    <a class="close-reveal-modal">Cerrar <img src="images/close.png"></a>
</div>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script src="js/jquery.reveal.js"></script>
</body>
</html>